﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Text;
using System.Security.Policy;

namespace Login
{
    public partial class BookInfo : Form
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True");
        Form2 f2;
        borrowinfo2 b2;
        public string id;
        public string s;
        public BookInfo(object o)
        {
            if (o is Form2)
            {
                f2=(Form2)o;
                this.id = f2.id;
                this.WindowState = f2.WindowState;
            }
            else if (o is borrowinfo2)
            {
                b2 = (borrowinfo2)o;
                this.id = b2.id;
                this.WindowState = b2.WindowState;
            }
            con.Open();
            InitializeComponent();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            String query = "select * from book where bookTitle like('%" + textBox5.Text + "%') or bookId ='" +
                           textBox5.Text + "'";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            textBox5.Clear();
            textBox5.Focus();
            String query = "select * from book where bookTitle like('%" + textBox5.Text + "%') or bookId ='" +
                           textBox5.Text + "'";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void BookInfo_Load(object sender, EventArgs e)
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
            book_info();
        }
        private void book_info()
        {
            String query = "select * from book";
            SqlDataAdapter sda = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            s = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            int j = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString());

            if (j > 0)
            { 
                this.Hide();
                borrowinfo2 b2 = new borrowinfo2(this);
                b2.Show();
            }
            else
            {
                MessageBox.Show("Book Is not avaiable");
            }

        }
        private void BookInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f2 = new Form2(this);
            f2.Show();
        }

        private void BookInfo_Activated(object sender, EventArgs e)
        {
            book_info();
        }
    }
}


