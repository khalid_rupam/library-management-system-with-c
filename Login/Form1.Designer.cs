﻿namespace Login
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.Password = new System.Windows.Forms.Label();
            this.textBoxpassword = new System.Windows.Forms.TextBox();
            this.createnewacc = new System.Windows.Forms.Label();
            this.signup = new System.Windows.Forms.LinkLabel();
            this.Id = new System.Windows.Forms.Label();
            this.btnchck = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnlogin = new System.Windows.Forms.Button();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxId
            // 
            this.textBoxId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxId.BackColor = System.Drawing.Color.White;
            this.textBoxId.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxId.ForeColor = System.Drawing.Color.Black;
            this.textBoxId.Location = new System.Drawing.Point(276, 224);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.Size = new System.Drawing.Size(311, 38);
            this.textBoxId.TabIndex = 1;
            // 
            // Password
            // 
            this.Password.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Password.AutoSize = true;
            this.Password.BackColor = System.Drawing.Color.Transparent;
            this.Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Password.ForeColor = System.Drawing.Color.White;
            this.Password.Location = new System.Drawing.Point(269, 279);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(98, 25);
            this.Password.TabIndex = 2;
            this.Password.Text = "Password";
            // 
            // textBoxpassword
            // 
            this.textBoxpassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxpassword.BackColor = System.Drawing.Color.White;
            this.textBoxpassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxpassword.ForeColor = System.Drawing.Color.Black;
            this.textBoxpassword.Location = new System.Drawing.Point(276, 308);
            this.textBoxpassword.Name = "textBoxpassword";
            this.textBoxpassword.PasswordChar = '*';
            this.textBoxpassword.Size = new System.Drawing.Size(256, 38);
            this.textBoxpassword.TabIndex = 3;
            // 
            // createnewacc
            // 
            this.createnewacc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.createnewacc.AutoSize = true;
            this.createnewacc.BackColor = System.Drawing.Color.Transparent;
            this.createnewacc.Font = new System.Drawing.Font("Palatino Linotype", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createnewacc.ForeColor = System.Drawing.Color.White;
            this.createnewacc.Location = new System.Drawing.Point(325, 411);
            this.createnewacc.Name = "createnewacc";
            this.createnewacc.Size = new System.Drawing.Size(213, 27);
            this.createnewacc.TabIndex = 7;
            this.createnewacc.Text = "Create a new account?";
            // 
            // signup
            // 
            this.signup.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.signup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.signup.AutoSize = true;
            this.signup.BackColor = System.Drawing.Color.Transparent;
            this.signup.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.signup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signup.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.signup.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.signup.LinkColor = System.Drawing.Color.Red;
            this.signup.Location = new System.Drawing.Point(385, 448);
            this.signup.Name = "signup";
            this.signup.Size = new System.Drawing.Size(79, 25);
            this.signup.TabIndex = 6;
            this.signup.TabStop = true;
            this.signup.Text = "Sign up";
            this.signup.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.signup_LinkClicked);
            // 
            // Id
            // 
            this.Id.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Id.AutoSize = true;
            this.Id.BackColor = System.Drawing.Color.Transparent;
            this.Id.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Id.ForeColor = System.Drawing.Color.White;
            this.Id.Location = new System.Drawing.Point(269, 198);
            this.Id.Name = "Id";
            this.Id.Size = new System.Drawing.Size(74, 25);
            this.Id.TabIndex = 0;
            this.Id.Text = "User Id";
            // 
            // btnchck
            // 
            this.btnchck.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnchck.BackColor = System.Drawing.Color.White;
            this.btnchck.Image = global::Login.Properties.Resources.Praveen_Minimal_Outline_View;
            this.btnchck.Location = new System.Drawing.Point(532, 308);
            this.btnchck.Name = "btnchck";
            this.btnchck.Size = new System.Drawing.Size(55, 38);
            this.btnchck.TabIndex = 9;
            this.btnchck.UseVisualStyleBackColor = false;
            this.btnchck.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnchck_MouseDown);
            this.btnchck.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnchck_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Login.Properties.Resources.icons8_customer_96;
            this.pictureBox1.Location = new System.Drawing.Point(359, 72);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(235, 106);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // btnlogin
            // 
            this.btnlogin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnlogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnlogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnlogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnlogin.Image = global::Login.Properties.Resources.sign_check_icon1;
            this.btnlogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnlogin.Location = new System.Drawing.Point(359, 370);
            this.btnlogin.Name = "btnlogin";
            this.btnlogin.Size = new System.Drawing.Size(139, 38);
            this.btnlogin.TabIndex = 4;
            this.btnlogin.Text = "Log In";
            this.btnlogin.UseVisualStyleBackColor = false;
            this.btnlogin.Click += new System.EventHandler(this.btnlogin_Click);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this;
            this.bunifuDragControl1.Vertical = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.BackgroundImage = global::Login.Properties.Resources.on_the_table_at_library_blurred_background_h0rouef1g_thumbnail_full011;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(850, 536);
            this.Controls.Add(this.btnchck);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Id);
            this.Controls.Add(this.createnewacc);
            this.Controls.Add(this.btnlogin);
            this.Controls.Add(this.signup);
            this.Controls.Add(this.textBoxId);
            this.Controls.Add(this.textBoxpassword);
            this.Controls.Add(this.Password);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.TransparencyKey = System.Drawing.Color.LightGray;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Id;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.Label Password;
        private System.Windows.Forms.TextBox textBoxpassword;
        private System.Windows.Forms.Button btnlogin;
        private System.Windows.Forms.LinkLabel signup;
        private System.Windows.Forms.Label createnewacc;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnchck;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
    }
}

