﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Login
{
    public partial class Form10 : Form
    {
        Form6 f6;
        public string id;
        public Form10(Form6 f6)
        {
            this.f6 = f6;
            this.id = f6.id;
            this.WindowState = f6.WindowState;
            InitializeComponent();
        }
        string conString = "Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True";
        public void load()
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select * from Employee";
                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.Fill;
            }
        }
        
        private void Form10_Load(object sender, EventArgs e)
        {
            load();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form6 f = new Form6(this);
            f.Show();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            textBoxempid.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBoxName.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBoxPhn.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBoxAdd.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            comboBoxtype.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            textBoxsal.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            textBoxmail.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
        }

        private void textBoxsearch_TextChanged(object sender, EventArgs e)
        {
            string value = textBoxsearch.Text.ToString();
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select * from Employee where Name like '" + value + "%'";
                //MessageBox.Show();
                if (comboBoxsearch.Text.ToString().Equals("Name"))
                {
                    query = "select * from Employee where Name like '" + value + "%'";
                }
                else if (comboBoxsearch.Text.ToString().Equals("Employee_Id"))
                {
                    query = "select * from Employee where Employee_Id like '" + value + "%'";
                }
                else if (comboBoxsearch.Text.ToString().Equals("PhoneNumber"))
                {
                    query = "select * from Employee where PhoneNumber like '" + value + "%'";
                }
                else if (comboBoxsearch.Text.ToString().Equals("Address"))
                {
                    query = "select * from Employee where Address like '" + value + "%'";
                }
                else if (comboBoxsearch.Text.ToString().Equals("Type"))
                {
                    query = "select * from Employee where Type like '" + value + "%'";
                }
                else if (comboBoxsearch.Text.ToString().Equals("Salary"))
                {
                    query = "select * from Employee where Salary like '" + value + "%'";
                }
                else if (comboBoxsearch.Text.ToString().Equals("Mail"))
                {
                    query = "select * from Employee where Mail like '" + value + "%'";
                }
                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.Fill;

            }
        }

        private void Form10_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                if (!textBoxempid.Text.ToString().Equals(""))
                {
                    int a;
                    //try
                    //{
                        int t = int.Parse(textBoxPhn.Text.ToString());
                        bool b = int.TryParse(textBoxName.Text.ToString(), out a);
                        bool c = int.TryParse(textBoxAdd.Text.ToString(), out a);
                        bool d = int.TryParse(textBoxsal.Text.ToString(), out a);
                        //MessageBox.Show(t.ToString());
                        if (t > 0 && textBoxPhn.Text.ToString().Length == 11)
                        {
                            if (b == false)
                            {
                                if (c == false)
                                {
                                    if (d == true)
                                    {
                                        string query = "update Employee set Name='" + textBoxName.Text.ToString() + "',PhoneNumber='" + textBoxPhn.Text.ToString() + "',Address='" + textBoxAdd.Text.ToString() + "',Type='" + comboBoxtype.Text.ToString() + "',Salary ='" + textBoxsal.Text.ToString() + "',Mail='" + textBoxmail.Text.ToString() + "' where Employee_Id ='" + textBoxempid.Text.ToString() + "'";

                                        SqlCommand sql1 = new SqlCommand(query, sql);
                                        sql1.ExecuteNonQuery();
                                        load();
                                        MessageBox.Show("<<Success>>", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                    }
                                    else
                                    {
                                        MessageBox.Show("Salary can not be a character");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Address can not be number only");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Name error");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Number Can not be negative\nNumber can not be less the 11");
                        }
                    //}
                    //catch (Exception)
                    //{
                    //    MessageBox.Show("invalid Number");
                    //}
                }
                else
                {
                    MessageBox.Show("No Data Found");
                }
            }

            else
            {
                MessageBox.Show("Not ok");
            }
        }

        private void BtnRefrash_Click(object sender, EventArgs e)
        {
            textBoxempid.Text = "";
            textBoxName.Text = "";
            textBoxPhn.Text = "";
            textBoxAdd.Text = "";
            comboBoxtype.Text = "";
            textBoxsal.Text = "";
            textBoxmail.Text = "";
            comboBoxsearch.Text = "";
            comboBoxtype.Text = "";

            load();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);

            sql.Open();

            if (sql.State == ConnectionState.Open)
            {
                if (!textBoxempid.Text.ToString().Equals(""))
                {
                    string query = "delete from Employee where Employee_Id ='" + textBoxempid.Text.ToString() + "'";
                    string query1 = "delete from login where Id ='" + textBoxempid.Text.ToString() + "'";
                    string query2 = "delete from Common where All_Id ='" + textBoxempid.Text.ToString() + "'";
                    SqlCommand sql1 = new SqlCommand(query, sql);
                    sql1.ExecuteNonQuery();
                    SqlCommand sql2 = new SqlCommand(query1, sql);
                    sql1.ExecuteNonQuery();
                    SqlCommand sql3 = new SqlCommand(query2, sql);
                    sql1.ExecuteNonQuery();
                    textBoxempid.Text = "";
                    textBoxName.Text = "";
                    textBoxPhn.Text = "";
                    textBoxAdd.Text = "";
                    comboBoxtype.Text = "";
                    textBoxsal.Text = "";
                    textBoxmail.Text = "";
                    comboBoxsearch.Text = "";
                    comboBoxtype.Text = "";
                    load();
                    MessageBox.Show("<<SUCCESS>>", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show("No Data Found");
                }
            }
            else
            {
                MessageBox.Show("Not ok");
            }
        }
    }
}
