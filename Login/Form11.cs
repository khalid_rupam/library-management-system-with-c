﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace Login
{
    public partial class Form11 : Form
    {
        Form6 f6;
        public string id;
        public Form11(Form6 f6)
        {
            this.f6 = f6;
            this.id = f6.id;
            InitializeComponent();
        }
        string conString = "Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True";
        private void Form11_Load(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select l.Id,e.Name,l.Type,l.Status,e.Mail from login l,Employee e where l.Id=e.Employee_Id";

                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.Fill;
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string value = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            if (value.Equals("Active"))
            {
                SqlConnection sql = new SqlConnection(conString);
                sql.Open();
                if (sql.State == ConnectionState.Open)
                {
                    SmtpClient clientDetais = new SmtpClient();
                    clientDetais.Port = 587;
                    clientDetais.Host = "smtp.gmail.com";
                    clientDetais.EnableSsl = true;
                    clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                    clientDetais.UseDefaultCredentials = false;
                    clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");
                    MailMessage maildetails = new MailMessage();
                    maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                    maildetails.To.Add(dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString());
                    maildetails.Subject = "Account BANNED";
                    maildetails.Body = "Welcome to our library " + dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString() + "\n Dear Employee,\n You Have Been BANNED for some time\n CONTACT librarymanagementSystemAIUB@gmail.com  ";
                    clientDetais.Send(maildetails);

                    string query = "update login set Status='Inactive' where Id ='" + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "'";
                    SqlCommand sql1 = new SqlCommand(query, sql);
                    sql1.ExecuteNonQuery();
                    MessageBox.Show("<<Success>>", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            else if (value.Equals("Inactive"))
            {
                SqlConnection sql = new SqlConnection(conString);
                sql.Open();
                if (sql.State == ConnectionState.Open)
                {
                    SmtpClient clientDetais = new SmtpClient();
                    clientDetais.Port = 587;
                    clientDetais.Host = "smtp.gmail.com";
                    clientDetais.EnableSsl = true;
                    clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                    clientDetais.UseDefaultCredentials = false;
                    clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");
                    MailMessage maildetails = new MailMessage();
                    maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                    maildetails.To.Add(dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString());
                    maildetails.Subject = "Account Activated";
                    maildetails.Body = "Welcome to our library " + dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString() + "\n Your Employee Id:" + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "\n YOU ARE NOW ACTIVATED\nYOU CAN USE OUR SYSTEM NOW";
                    clientDetais.Send(maildetails);

                    string query = "update login set Status='Active' where Id ='" + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "'";
                    SqlCommand sql1 = new SqlCommand(query, sql);
                    sql1.ExecuteNonQuery();
                    MessageBox.Show("<<Success>>", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            SqlConnection sql2 = new SqlConnection(conString);
            sql2.Open();
            if (sql2.State == ConnectionState.Open)
            {
                string query = "select l.Id,e.Name,l.Type,l.Status,e.Mail from login l,Employee e where l.Id=e.Employee_Id";

                SqlCommand sql1 = new SqlCommand(query, sql2);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql2);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.Fill;
                comboBox1.Text = "";
            }
            MessageBox.Show(value);
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text.ToString().Equals("Active"))
            {
                SqlConnection sql = new SqlConnection(conString);
                sql.Open();
                if (sql.State == ConnectionState.Open)
                {
                    string query = "select l.Id,e.Name,l.Type,l.Status,e.Mail from login l,Employee e where l.Id=e.Employee_Id and l.Status ='Active'";

                    SqlCommand sql1 = new SqlCommand(query, sql);
                    SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    dataGridView1.DataSource = dt;
                    dataGridView1.AllowUserToAddRows = false;
                    dataGridView1.AllowUserToDeleteRows = false;
                    dataGridView1.AllowUserToResizeRows = false;
                    dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                    dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                    dataGridView1.AutoSizeColumnsMode =
                        DataGridViewAutoSizeColumnsMode.Fill;


                }
            }
            else if (comboBox1.Text.ToString().Equals("Inactive"))
            {
                SqlConnection sql = new SqlConnection(conString);
                sql.Open();
                if (sql.State == ConnectionState.Open)
                {
                    string query = "select l.Id,e.Name,l.Type,l.Status,e.Mail from login l,Employee e where l.Id=e.Employee_Id and l.Status ='Inactive'";

                    SqlCommand sql1 = new SqlCommand(query, sql);
                    SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    dataGridView1.DataSource = dt;
                    dataGridView1.AllowUserToAddRows = false;
                    dataGridView1.AllowUserToDeleteRows = false;
                    dataGridView1.AllowUserToResizeRows = false;
                    dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                    dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                    dataGridView1.AutoSizeColumnsMode =
                        DataGridViewAutoSizeColumnsMode.Fill;

                }

            }
            else if (comboBox1.Text.ToString().Equals("All"))
            {
                SqlConnection sql = new SqlConnection(conString);
                sql.Open();
                if (sql.State == ConnectionState.Open)
                {
                    string query = "select l.Id,e.Name,l.Type,l.Status,e.Mail from login l,Employee e where l.Id=e.Employee_Id";

                    SqlCommand sql1 = new SqlCommand(query, sql);
                    SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    dataGridView1.DataSource = dt;
                    dataGridView1.AllowUserToAddRows = false;
                    dataGridView1.AllowUserToDeleteRows = false;
                    dataGridView1.AllowUserToResizeRows = false;
                    dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                    dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                    dataGridView1.AutoSizeColumnsMode =
                        DataGridViewAutoSizeColumnsMode.Fill;

                }

            }
        }

        private void btnback_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form6 f = new Form6(this);
            f.Show();

        }

        private void Form11_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
