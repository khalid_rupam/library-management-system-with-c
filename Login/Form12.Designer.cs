﻿namespace Login
{
    partial class Form12
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnfindbk = new System.Windows.Forms.Button();
            this.btnborrow = new System.Windows.Forms.Button();
            this.btnchangepass = new System.Windows.Forms.Button();
            this.btnminfo = new System.Windows.Forms.Button();
            this.btnlogout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Palatino Linotype", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.SystemColors.Control;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(279, 59);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(444, 53);
            this.bunifuCustomLabel1.TabIndex = 4;
            this.bunifuCustomLabel1.Text = "Welcome to our Library";
            // 
            // btnfindbk
            // 
            this.btnfindbk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnfindbk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnfindbk.Location = new System.Drawing.Point(337, 194);
            this.btnfindbk.Name = "btnfindbk";
            this.btnfindbk.Size = new System.Drawing.Size(103, 51);
            this.btnfindbk.TabIndex = 7;
            this.btnfindbk.Text = "Find Book";
            this.btnfindbk.UseVisualStyleBackColor = false;
            this.btnfindbk.Click += new System.EventHandler(this.btnfindbk_Click);
            // 
            // btnborrow
            // 
            this.btnborrow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnborrow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnborrow.Location = new System.Drawing.Point(337, 296);
            this.btnborrow.Name = "btnborrow";
            this.btnborrow.Size = new System.Drawing.Size(103, 51);
            this.btnborrow.TabIndex = 8;
            this.btnborrow.Text = "Borrowinfo";
            this.btnborrow.UseVisualStyleBackColor = false;
            this.btnborrow.Click += new System.EventHandler(this.btnborrow_Click);
            // 
            // btnchangepass
            // 
            this.btnchangepass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnchangepass.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnchangepass.Location = new System.Drawing.Point(529, 296);
            this.btnchangepass.Name = "btnchangepass";
            this.btnchangepass.Size = new System.Drawing.Size(103, 51);
            this.btnchangepass.TabIndex = 9;
            this.btnchangepass.Text = "Change Password";
            this.btnchangepass.UseVisualStyleBackColor = false;
            this.btnchangepass.Click += new System.EventHandler(this.btnchangepass_Click);
            // 
            // btnminfo
            // 
            this.btnminfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnminfo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnminfo.Location = new System.Drawing.Point(529, 194);
            this.btnminfo.Name = "btnminfo";
            this.btnminfo.Size = new System.Drawing.Size(103, 51);
            this.btnminfo.TabIndex = 10;
            this.btnminfo.Text = "My info";
            this.btnminfo.UseVisualStyleBackColor = false;
            this.btnminfo.Click += new System.EventHandler(this.btnminfo_Click);
            // 
            // btnlogout
            // 
            this.btnlogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnlogout.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnlogout.Location = new System.Drawing.Point(880, 12);
            this.btnlogout.Name = "btnlogout";
            this.btnlogout.Size = new System.Drawing.Size(103, 51);
            this.btnlogout.TabIndex = 11;
            this.btnlogout.Text = "Sign out";
            this.btnlogout.UseVisualStyleBackColor = false;
            this.btnlogout.Click += new System.EventHandler(this.btnlogout_Click);
            // 
            // Form12
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Login.Properties.Resources.on_the_table_at_library_blurred_background_h0rouef1g_thumbnail_full01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(995, 536);
            this.Controls.Add(this.btnlogout);
            this.Controls.Add(this.btnminfo);
            this.Controls.Add(this.btnchangepass);
            this.Controls.Add(this.btnborrow);
            this.Controls.Add(this.btnfindbk);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Name = "Form12";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Home";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form12_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private System.Windows.Forms.Button btnfindbk;
        private System.Windows.Forms.Button btnborrow;
        private System.Windows.Forms.Button btnchangepass;
        private System.Windows.Forms.Button btnminfo;
        private System.Windows.Forms.Button btnlogout;
    }
}