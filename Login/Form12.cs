﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Login
{
    public partial class Form12 : Form
    {
        Form1 f1;
        Form13 f13;
        Form14 f14;
        Form16 f16;
        Form17 f17;
        public string id;
        public Form12(object f)
        {
            if (f is Form1)
            {
                this.f1 = (Form1)f;
                this.id = f1.id;
                this.WindowState = f1.WindowState;
            }
            else if (f is Form13)
            {
                this.f13 = (Form13)f;
                this.id = f13.id;
                this.WindowState = f13.WindowState;
            }
            else if (f is Form16)
            {
                this.f16 = (Form16)f;
                this.id = f16.id;
                this.WindowState = f16.WindowState;
            }
            else if (f is Form17)
            {
                this.f17 = (Form17)f;
                this.id = f17.id;
                this.WindowState = f17.WindowState;
            }
            else if (f is Form14)
            {
                this.f14 = (Form14)f;
                this.id = f14.id;
                this.WindowState = f14.WindowState;
            }
            MessageBox.Show(id);
            InitializeComponent();
        }
        

        private void Form12_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        
        private void btnfindbk_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form13 f13 = new Form13(this);
            f13.Show();
        }

        private void btnborrow_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form16 f16 = new Form16(this);
            f16.Show();
        }

        private void btnminfo_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form14 f14 = new Form14(this);
            f14.Show();
        }

        private void btnchangepass_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form17 f17 = new Form17(this);
            f17.Show();
        }

        private void btnlogout_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f5 = new Form1();
            f5.Show();
        }
    }
}
