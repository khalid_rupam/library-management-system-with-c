﻿namespace Login
{
    partial class Form14
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelid = new System.Windows.Forms.Label();
            this.txtmail = new System.Windows.Forms.TextBox();
            this.labelpasswrd = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelname = new System.Windows.Forms.Label();
            this.btnback = new System.Windows.Forms.Button();
            this.labelphn = new System.Windows.Forms.Label();
            this.btnup = new System.Windows.Forms.Button();
            this.textBoxid = new System.Windows.Forms.TextBox();
            this.TxtType = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.labeladd = new System.Windows.Forms.Label();
            this.txtPhn = new System.Windows.Forms.TextBox();
            this.txtAdd = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelid
            // 
            this.labelid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelid.AutoSize = true;
            this.labelid.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelid.Location = new System.Drawing.Point(69, 42);
            this.labelid.Name = "labelid";
            this.labelid.Size = new System.Drawing.Size(171, 37);
            this.labelid.TabIndex = 27;
            this.labelid.Text = "Customer Id";
            // 
            // txtmail
            // 
            this.txtmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmail.Location = new System.Drawing.Point(315, 301);
            this.txtmail.Name = "txtmail";
            this.txtmail.Size = new System.Drawing.Size(241, 34);
            this.txtmail.TabIndex = 40;
            // 
            // labelpasswrd
            // 
            this.labelpasswrd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelpasswrd.AutoSize = true;
            this.labelpasswrd.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelpasswrd.Location = new System.Drawing.Point(69, 89);
            this.labelpasswrd.Name = "labelpasswrd";
            this.labelpasswrd.Size = new System.Drawing.Size(91, 37);
            this.labelpasswrd.TabIndex = 28;
            this.labelpasswrd.Text = "Name";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 301);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 37);
            this.label1.TabIndex = 39;
            this.label1.Text = "Email";
            // 
            // labelname
            // 
            this.labelname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelname.AutoSize = true;
            this.labelname.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelname.Location = new System.Drawing.Point(69, 138);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(201, 37);
            this.labelname.TabIndex = 29;
            this.labelname.Text = "Phone Number";
            // 
            // btnback
            // 
            this.btnback.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnback.Location = new System.Drawing.Point(449, 366);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(107, 49);
            this.btnback.TabIndex = 38;
            this.btnback.Text = "Back";
            this.btnback.UseVisualStyleBackColor = false;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // labelphn
            // 
            this.labelphn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelphn.AutoSize = true;
            this.labelphn.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelphn.Location = new System.Drawing.Point(69, 188);
            this.labelphn.Name = "labelphn";
            this.labelphn.Size = new System.Drawing.Size(121, 37);
            this.labelphn.TabIndex = 30;
            this.labelphn.Text = "Address";
            // 
            // btnup
            // 
            this.btnup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnup.Location = new System.Drawing.Point(87, 366);
            this.btnup.Name = "btnup";
            this.btnup.Size = new System.Drawing.Size(107, 49);
            this.btnup.TabIndex = 37;
            this.btnup.Text = "update";
            this.btnup.UseVisualStyleBackColor = false;
            this.btnup.Click += new System.EventHandler(this.btnup_Click);
            // 
            // textBoxid
            // 
            this.textBoxid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxid.Enabled = false;
            this.textBoxid.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxid.Location = new System.Drawing.Point(315, 42);
            this.textBoxid.Name = "textBoxid";
            this.textBoxid.Size = new System.Drawing.Size(241, 34);
            this.textBoxid.TabIndex = 31;
            // 
            // TxtType
            // 
            this.TxtType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TxtType.Enabled = false;
            this.TxtType.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtType.Location = new System.Drawing.Point(315, 244);
            this.TxtType.Name = "TxtType";
            this.TxtType.Size = new System.Drawing.Size(241, 34);
            this.TxtType.TabIndex = 36;
            // 
            // txtName
            // 
            this.txtName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(315, 96);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(241, 34);
            this.txtName.TabIndex = 32;
            // 
            // labeladd
            // 
            this.labeladd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labeladd.AutoSize = true;
            this.labeladd.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeladd.Location = new System.Drawing.Point(69, 244);
            this.labeladd.Name = "labeladd";
            this.labeladd.Size = new System.Drawing.Size(78, 37);
            this.labeladd.TabIndex = 35;
            this.labeladd.Text = "Type";
            // 
            // txtPhn
            // 
            this.txtPhn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtPhn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhn.Location = new System.Drawing.Point(315, 148);
            this.txtPhn.Name = "txtPhn";
            this.txtPhn.Size = new System.Drawing.Size(241, 34);
            this.txtPhn.TabIndex = 33;
            // 
            // txtAdd
            // 
            this.txtAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdd.Location = new System.Drawing.Point(315, 195);
            this.txtAdd.Name = "txtAdd";
            this.txtAdd.Size = new System.Drawing.Size(241, 34);
            this.txtAdd.TabIndex = 34;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelete.Location = new System.Drawing.Point(266, 366);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(107, 49);
            this.btnDelete.TabIndex = 41;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.textBoxid);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.txtAdd);
            this.groupBox1.Controls.Add(this.labelid);
            this.groupBox1.Controls.Add(this.txtPhn);
            this.groupBox1.Controls.Add(this.txtmail);
            this.groupBox1.Controls.Add(this.labeladd);
            this.groupBox1.Controls.Add(this.labelpasswrd);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TxtType);
            this.groupBox1.Controls.Add(this.labelname);
            this.groupBox1.Controls.Add(this.btnup);
            this.groupBox1.Controls.Add(this.btnback);
            this.groupBox1.Controls.Add(this.labelphn);
            this.groupBox1.Location = new System.Drawing.Point(214, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(589, 455);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "My Information";
            // 
            // Form14
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Login.Properties.Resources.on_the_table_at_library_blurred_background_h0rouef1g_thumbnail_full01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(995, 536);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form14";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "My Info";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form14_FormClosing);
            this.Load += new System.EventHandler(this.Form14_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelid;
        private System.Windows.Forms.TextBox txtmail;
        private System.Windows.Forms.Label labelpasswrd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Label labelphn;
        private System.Windows.Forms.Button btnup;
        private System.Windows.Forms.TextBox textBoxid;
        private System.Windows.Forms.TextBox TxtType;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label labeladd;
        private System.Windows.Forms.TextBox txtPhn;
        private System.Windows.Forms.TextBox txtAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox groupBox1;


    }
}