﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Login
{
    public partial class Form14 : Form
    {
        Form12 f12;
        public string id;
        public Form14(Form12 f12)
        {
            this.f12 = f12;
            this.id = f12.id;
            this.WindowState = f12.WindowState;
            InitializeComponent();
        }
        string conString = "Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True";
        private void Form14_Load(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select * from CustomerData where Customer_Id = '" + f12.id + "'";
                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataReader reader = sql1.ExecuteReader();
                //  bool j = reader.Read();
                //MessageBox.Show(reader.Read().ToString());
                while (reader.Read())
                {
                    textBoxid.Text = reader[0].ToString();
                    txtName.Text = reader[1].ToString();
                    txtAdd.Text = reader[2].ToString();
                    txtPhn.Text = reader[3].ToString();
                    TxtType.Text = reader[4].ToString();
                    txtmail.Text = reader[5].ToString();
                }
                if (reader.Read())
                {
                    MessageBox.Show("Invalid Id or Password");
                }
                else { }
            }
            else
            {
                MessageBox.Show("Not ok");
            }
        }

        private void btnup_Click(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (!txtName.Text.ToString().Equals("") && !txtPhn.Text.ToString().Equals("") && !txtAdd.Text.ToString().Equals(""))
            {
                try
                {
                    int i = int.Parse(txtPhn.Text.ToString());
                    bool j = int.TryParse(txtName.Text.ToString(), out i);
                    bool o = int.TryParse(txtName.Text.ToString().Substring(0, 1), out i);
                    bool k = int.TryParse(txtAdd.Text.ToString(), out i);
                    bool p = int.TryParse(txtmail.Text.ToString(), out i);
                    
                    string r = txtPhn.Text.ToString().Substring(0, 3);
                    if (txtPhn.Text.ToString().Length == 11 && i >= 0)
                    {
                        if (r.Equals("017") || r.Equals("019") || r.Equals("015") || r.Equals("018"))
                        {
                            if (j == false)
                            {
                                if (k == false)
                                {
                                    if (o == false)
                                    {
                                        if (p == false)
                                        {
                                            if (MessageBox.Show("Are you sure?", "Sure", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                                            {
                                                string query1 = "update CustomerData set Name = '" + txtName.Text.ToString() + "',Address = '" + txtAdd.Text.ToString() + "',PhoneNumber = '" + txtPhn.Text.ToString() + "',Email='" + txtmail.Text.ToString() + "' where Customer_Id = '" + textBoxid.Text.ToString() + "'";
                                                SqlCommand cmd = new SqlCommand(query1, sql);
                                                cmd = new SqlCommand(query1, sql);
                                                cmd.ExecuteNonQuery();
                                                MessageBox.Show("<<Success>>");
                                                this.Hide();
                                                this.Show();
                                            }
                                            else { }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Invalid Mail");
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("invalid name");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Invalid address");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Invalid Name <<Number Cannot be a name>>");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid Operator");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Phone Number is less then 11");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex.Message.ToString());
                }
            }
            else
            {
                MessageBox.Show("Invalid input or incomplete field");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);

            sql.Open();

            if (sql.State == ConnectionState.Open)
            {
                string query3 = "select COUNT(*) from borrowinfo where Customer_Id ='" + textBoxid.Text.ToString() + "'";
                SqlCommand sql3 = new SqlCommand(query3, sql);
                SqlDataReader reader = sql3.ExecuteReader();
                if (reader.Read())
                {
                    if (int.Parse(reader[0].ToString()) == 0)
                    {
                        sql.Close();
                        sql.Open();
                        string query = "delete from CustomerData where Customer_Id ='" + textBoxid.Text.ToString() + "'";
                        SqlCommand sql1 = new SqlCommand(query, sql);
                        sql1.ExecuteNonQuery();
                        string query4 = "delete from login where Id ='" + textBoxid.Text.ToString() + "'";
                        SqlCommand sql4 = new SqlCommand(query4, sql);
                        sql4.ExecuteNonQuery();
                        string query2 = "delete from Common where All_Id ='"+textBoxid.Text.ToString()+"'";
                        SqlCommand sql2 = new SqlCommand(query2, sql);
                        sql2.ExecuteNonQuery();
                        this.Hide();
                        Form1 f1 = new Form1();
                        f1.Show();
                        MessageBox.Show("<<SUCCESS>>", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    else
                    {
                        MessageBox.Show("Access denied!!");
                    }
                }
                
                
            }
            else
            {
                MessageBox.Show("Not ok");
            }
        }

        private void btnback_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form12 f12 = new Form12(this);
            f12.Show();
        }

        private void Form14_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
