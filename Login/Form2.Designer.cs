﻿namespace Login
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnmngemp = new System.Windows.Forms.Button();
            this.btnBorrow = new System.Windows.Forms.Button();
            this.btnbookinfo = new System.Windows.Forms.Button();
            this.btnMyInfo = new System.Windows.Forms.Button();
            this.btnReurn = new System.Windows.Forms.Button();
            this.btnCustomerInfo = new System.Windows.Forms.Button();
            this.btnAddBook = new System.Windows.Forms.Button();
            this.btnsignout = new System.Windows.Forms.Button();
            this.btnpasswrd = new System.Windows.Forms.Button();
            this.Wlcm = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAccept.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccept.ForeColor = System.Drawing.Color.Ivory;
            this.btnAccept.Location = new System.Drawing.Point(311, 78);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(103, 51);
            this.btnAccept.TabIndex = 1;
            this.btnAccept.Text = "Activated";
            this.btnAccept.UseVisualStyleBackColor = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnmngemp
            // 
            this.btnmngemp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnmngemp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnmngemp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnmngemp.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmngemp.ForeColor = System.Drawing.Color.Ivory;
            this.btnmngemp.Location = new System.Drawing.Point(311, 149);
            this.btnmngemp.Name = "btnmngemp";
            this.btnmngemp.Size = new System.Drawing.Size(103, 51);
            this.btnmngemp.TabIndex = 2;
            this.btnmngemp.Text = "Manage Employee";
            this.btnmngemp.UseVisualStyleBackColor = false;
            this.btnmngemp.Click += new System.EventHandler(this.btnmngemp_Click);
            // 
            // btnBorrow
            // 
            this.btnBorrow.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBorrow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnBorrow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBorrow.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBorrow.ForeColor = System.Drawing.Color.Ivory;
            this.btnBorrow.Location = new System.Drawing.Point(311, 225);
            this.btnBorrow.Name = "btnBorrow";
            this.btnBorrow.Size = new System.Drawing.Size(103, 51);
            this.btnBorrow.TabIndex = 3;
            this.btnBorrow.Text = "Borrow";
            this.btnBorrow.UseVisualStyleBackColor = false;
            this.btnBorrow.Click += new System.EventHandler(this.btnBorrow_Click);
            // 
            // btnbookinfo
            // 
            this.btnbookinfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnbookinfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnbookinfo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnbookinfo.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbookinfo.ForeColor = System.Drawing.Color.Ivory;
            this.btnbookinfo.Location = new System.Drawing.Point(311, 297);
            this.btnbookinfo.Name = "btnbookinfo";
            this.btnbookinfo.Size = new System.Drawing.Size(103, 51);
            this.btnbookinfo.TabIndex = 4;
            this.btnbookinfo.Text = "Book Info";
            this.btnbookinfo.UseVisualStyleBackColor = false;
            this.btnbookinfo.Click += new System.EventHandler(this.btnbookinfo_Click);
            // 
            // btnMyInfo
            // 
            this.btnMyInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnMyInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnMyInfo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMyInfo.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMyInfo.ForeColor = System.Drawing.Color.Ivory;
            this.btnMyInfo.Location = new System.Drawing.Point(482, 78);
            this.btnMyInfo.Name = "btnMyInfo";
            this.btnMyInfo.Size = new System.Drawing.Size(103, 51);
            this.btnMyInfo.TabIndex = 5;
            this.btnMyInfo.Text = "My Info\r\n";
            this.btnMyInfo.UseVisualStyleBackColor = false;
            this.btnMyInfo.Click += new System.EventHandler(this.btnMyInfo_Click);
            // 
            // btnReurn
            // 
            this.btnReurn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnReurn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnReurn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReurn.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReurn.ForeColor = System.Drawing.Color.Ivory;
            this.btnReurn.Location = new System.Drawing.Point(482, 225);
            this.btnReurn.Name = "btnReurn";
            this.btnReurn.Size = new System.Drawing.Size(103, 51);
            this.btnReurn.TabIndex = 6;
            this.btnReurn.Text = "Return";
            this.btnReurn.UseVisualStyleBackColor = false;
            this.btnReurn.Click += new System.EventHandler(this.btnReurn_Click);
            // 
            // btnCustomerInfo
            // 
            this.btnCustomerInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCustomerInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnCustomerInfo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCustomerInfo.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomerInfo.ForeColor = System.Drawing.Color.Ivory;
            this.btnCustomerInfo.Location = new System.Drawing.Point(482, 149);
            this.btnCustomerInfo.Name = "btnCustomerInfo";
            this.btnCustomerInfo.Size = new System.Drawing.Size(103, 51);
            this.btnCustomerInfo.TabIndex = 7;
            this.btnCustomerInfo.Text = "Customer Info";
            this.btnCustomerInfo.UseVisualStyleBackColor = false;
            this.btnCustomerInfo.Click += new System.EventHandler(this.btnCustomerInfo_Click);
            // 
            // btnAddBook
            // 
            this.btnAddBook.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddBook.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnAddBook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddBook.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddBook.ForeColor = System.Drawing.Color.Ivory;
            this.btnAddBook.Location = new System.Drawing.Point(482, 297);
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(103, 51);
            this.btnAddBook.TabIndex = 8;
            this.btnAddBook.Text = "Add Book";
            this.btnAddBook.UseVisualStyleBackColor = false;
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // btnsignout
            // 
            this.btnsignout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnsignout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnsignout.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsignout.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsignout.ForeColor = System.Drawing.SystemColors.Control;
            this.btnsignout.Location = new System.Drawing.Point(805, 12);
            this.btnsignout.Name = "btnsignout";
            this.btnsignout.Size = new System.Drawing.Size(91, 43);
            this.btnsignout.TabIndex = 9;
            this.btnsignout.Text = "sign out";
            this.btnsignout.UseVisualStyleBackColor = false;
            this.btnsignout.Click += new System.EventHandler(this.button9_Click);
            // 
            // btnpasswrd
            // 
            this.btnpasswrd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnpasswrd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnpasswrd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnpasswrd.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpasswrd.ForeColor = System.Drawing.Color.Ivory;
            this.btnpasswrd.Location = new System.Drawing.Point(311, 376);
            this.btnpasswrd.Name = "btnpasswrd";
            this.btnpasswrd.Size = new System.Drawing.Size(103, 51);
            this.btnpasswrd.TabIndex = 10;
            this.btnpasswrd.Text = "Change Password";
            this.btnpasswrd.UseVisualStyleBackColor = false;
            this.btnpasswrd.Click += new System.EventHandler(this.btnpasswrd_Click_1);
            // 
            // Wlcm
            // 
            this.Wlcm.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Wlcm.AutoSize = true;
            this.Wlcm.BackColor = System.Drawing.Color.Transparent;
            this.Wlcm.Font = new System.Drawing.Font("Palatino Linotype", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Wlcm.ForeColor = System.Drawing.SystemColors.Control;
            this.Wlcm.Location = new System.Drawing.Point(273, 14);
            this.Wlcm.Name = "Wlcm";
            this.Wlcm.Size = new System.Drawing.Size(376, 45);
            this.Wlcm.TabIndex = 0;
            this.Wlcm.Text = "Wellcome to our library";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.BackgroundImage = global::Login.Properties.Resources.on_the_table_at_library_blurred_background_h0rouef1g_thumbnail_full01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(908, 448);
            this.Controls.Add(this.btnpasswrd);
            this.Controls.Add(this.btnsignout);
            this.Controls.Add(this.btnAddBook);
            this.Controls.Add(this.btnCustomerInfo);
            this.Controls.Add(this.btnReurn);
            this.Controls.Add(this.btnMyInfo);
            this.Controls.Add(this.btnbookinfo);
            this.Controls.Add(this.btnBorrow);
            this.Controls.Add(this.btnmngemp);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.Wlcm);
            this.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Home";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnmngemp;
        private System.Windows.Forms.Button btnBorrow;
        private System.Windows.Forms.Button btnbookinfo;
        private System.Windows.Forms.Button btnMyInfo;
        private System.Windows.Forms.Button btnReurn;
        private System.Windows.Forms.Button btnCustomerInfo;
        private System.Windows.Forms.Button btnAddBook;
        private System.Windows.Forms.Button btnsignout;
        private System.Windows.Forms.Button btnpasswrd;
        private System.Windows.Forms.Label Wlcm;
    }
}