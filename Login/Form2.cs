﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Login
{
    public partial class Form2 : Form
    {
        Form1 f1;
        Form4 f2;
        Form6 f6;
        Form8 f8;
        Form9 f9;
        Tashnova t;
        BookInfo b;
        Srijony s;
        Return r;
        public string id;
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(object f)
        {
            if (f is Form1)
            {
                f1 = (Form1)f;
                id = f1.id;
                this.WindowState = f1.WindowState;
            }
            else if (f is Form4)
            {
                f2 = (Form4)f;
                id = f2.id;
                this.WindowState = f2.WindowState;
            }
            else if (f is Form6)
            {
                f6 = (Form6)f;
                id = f6.id;
                this.WindowState = f6.WindowState;
            }
            else if (f is Form8)
            {
                f8 = (Form8)f;
                id = f8.id;
                this.WindowState = f8.WindowState;
            }
            else if (f is Form9)
            {
                f9 = (Form9)f;
                id = f9.id;
                this.WindowState = f9.WindowState;
            }
            else if (f is Srijony)
            {
                s = (Srijony)f;
                id = s.id;
                this.WindowState = s.WindowState;
            }
            else if (f is BookInfo)
            {
                b = (BookInfo)f;
                id = b.id;
                this.WindowState = b.WindowState;
            }
            else if (f is Return)
            {
                r = (Return)f;
                this.id = r.id;
                this.WindowState = r.WindowState;
            }
            else if (f is Tashnova)
            {
                t = (Tashnova)f;
                this.id = t.id;
                this.WindowState = t.WindowState;
            }
            MessageBox.Show(id);
            
            InitializeComponent();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f5 = new Form1();
            f5.Show();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 f4 = new Form4(this);
            f4.Show();
        }

        private void btnMyInfo_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form5 f5 = new Form5(this);
            f5.Show();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        string conString = "Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True";
        private void btnmngemp_Click(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);

            sql.Open();

            if (sql.State == ConnectionState.Open)
            {

                string query = "select Type from Employee where Employee_Id='"+id+"'";

                SqlCommand sql1 = new SqlCommand(query, sql);

                SqlDataReader reader = sql1.ExecuteReader();
                
                while (reader.Read())
                {
                    if (reader[0].ToString().ToUpper().Equals("MANAGER"))
                    {
                        this.Hide();
                        Form6 f5 = new Form6(this);
                        f5.Show();
                    }
                    else
                    {
                        MessageBox.Show("Access Denied","",MessageBoxButtons.OKCancel,MessageBoxIcon.Stop);
                    }
                }
                if (reader.Read())
                {
                    MessageBox.Show("Invalid Id or Password");
                }
                else { }

            }
            else
            {
                MessageBox.Show("Not ok");
            }
            
        }
        
        private void btnbookinfo_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form8 f8 = new Form8(this);
            f8.Show();
        }

        private void btnAddBook_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form9 f9 = new Form9(this);
            f9.Show();
        }

        private void btnBorrow_Click(object sender, EventArgs e)
        {
            this.Hide();
            BookInfo b2 = new BookInfo(this);
            b2.Show();
        }

        private void btnCustomerInfo_Click(object sender, EventArgs e)
        {
            this.Hide();
            Srijony s = new Srijony(this);
            s.Show();
        }

        private void btnpasswrd_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnReurn_Click(object sender, EventArgs e)
        {
            this.Hide();
            Return r = new Return(this);
            r.Show();
        }

        private void btnpasswrd_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Tashnova t = new Tashnova(this);
            t.Show();
        }

        
    }
}
