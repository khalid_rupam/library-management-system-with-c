﻿namespace Login
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.labelid = new System.Windows.Forms.Label();
            this.labelpasswrd = new System.Windows.Forms.Label();
            this.labelname = new System.Windows.Forms.Label();
            this.labelphn = new System.Windows.Forms.Label();
            this.textBoxid = new System.Windows.Forms.TextBox();
            this.textBoxpass = new System.Windows.Forms.TextBox();
            this.textBoxname = new System.Windows.Forms.TextBox();
            this.textBoxphn = new System.Windows.Forms.TextBox();
            this.labeladd = new System.Windows.Forms.Label();
            this.textBoxadd = new System.Windows.Forms.TextBox();
            this.btnsignup = new System.Windows.Forms.Button();
            this.btnback = new System.Windows.Forms.Button();
            this.textBoxmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelid
            // 
            this.labelid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelid.AutoSize = true;
            this.labelid.BackColor = System.Drawing.Color.Transparent;
            this.labelid.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelid.ForeColor = System.Drawing.Color.White;
            this.labelid.Location = new System.Drawing.Point(132, 120);
            this.labelid.Name = "labelid";
            this.labelid.Size = new System.Drawing.Size(109, 37);
            this.labelid.TabIndex = 0;
            this.labelid.Text = "User Id";
            // 
            // labelpasswrd
            // 
            this.labelpasswrd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelpasswrd.AutoSize = true;
            this.labelpasswrd.BackColor = System.Drawing.Color.Transparent;
            this.labelpasswrd.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelpasswrd.ForeColor = System.Drawing.Color.White;
            this.labelpasswrd.Location = new System.Drawing.Point(132, 167);
            this.labelpasswrd.Name = "labelpasswrd";
            this.labelpasswrd.Size = new System.Drawing.Size(137, 37);
            this.labelpasswrd.TabIndex = 1;
            this.labelpasswrd.Text = "Password";
            // 
            // labelname
            // 
            this.labelname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelname.AutoSize = true;
            this.labelname.BackColor = System.Drawing.Color.Transparent;
            this.labelname.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelname.ForeColor = System.Drawing.Color.White;
            this.labelname.Location = new System.Drawing.Point(132, 216);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(91, 37);
            this.labelname.TabIndex = 2;
            this.labelname.Text = "Name";
            // 
            // labelphn
            // 
            this.labelphn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelphn.AutoSize = true;
            this.labelphn.BackColor = System.Drawing.Color.Transparent;
            this.labelphn.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelphn.ForeColor = System.Drawing.Color.White;
            this.labelphn.Location = new System.Drawing.Point(132, 266);
            this.labelphn.Name = "labelphn";
            this.labelphn.Size = new System.Drawing.Size(201, 37);
            this.labelphn.TabIndex = 3;
            this.labelphn.Text = "Phone Number";
            // 
            // textBoxid
            // 
            this.textBoxid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxid.Enabled = false;
            this.textBoxid.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxid.Location = new System.Drawing.Point(369, 120);
            this.textBoxid.Name = "textBoxid";
            this.textBoxid.Size = new System.Drawing.Size(241, 34);
            this.textBoxid.TabIndex = 4;
            // 
            // textBoxpass
            // 
            this.textBoxpass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxpass.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxpass.Location = new System.Drawing.Point(369, 174);
            this.textBoxpass.Name = "textBoxpass";
            this.textBoxpass.Size = new System.Drawing.Size(241, 34);
            this.textBoxpass.TabIndex = 5;
            // 
            // textBoxname
            // 
            this.textBoxname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxname.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxname.Location = new System.Drawing.Point(369, 226);
            this.textBoxname.Name = "textBoxname";
            this.textBoxname.Size = new System.Drawing.Size(241, 34);
            this.textBoxname.TabIndex = 6;
            // 
            // textBoxphn
            // 
            this.textBoxphn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxphn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxphn.Location = new System.Drawing.Point(369, 273);
            this.textBoxphn.Name = "textBoxphn";
            this.textBoxphn.Size = new System.Drawing.Size(241, 34);
            this.textBoxphn.TabIndex = 7;
            // 
            // labeladd
            // 
            this.labeladd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labeladd.AutoSize = true;
            this.labeladd.BackColor = System.Drawing.Color.Transparent;
            this.labeladd.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeladd.ForeColor = System.Drawing.Color.White;
            this.labeladd.Location = new System.Drawing.Point(132, 322);
            this.labeladd.Name = "labeladd";
            this.labeladd.Size = new System.Drawing.Size(121, 37);
            this.labeladd.TabIndex = 8;
            this.labeladd.Text = "Address";
            // 
            // textBoxadd
            // 
            this.textBoxadd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxadd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxadd.Location = new System.Drawing.Point(369, 322);
            this.textBoxadd.Name = "textBoxadd";
            this.textBoxadd.Size = new System.Drawing.Size(241, 34);
            this.textBoxadd.TabIndex = 9;
            // 
            // btnsignup
            // 
            this.btnsignup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnsignup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnsignup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnsignup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsignup.Location = new System.Drawing.Point(171, 434);
            this.btnsignup.Name = "btnsignup";
            this.btnsignup.Size = new System.Drawing.Size(107, 49);
            this.btnsignup.TabIndex = 11;
            this.btnsignup.Text = "sign up";
            this.btnsignup.UseVisualStyleBackColor = false;
            this.btnsignup.Click += new System.EventHandler(this.btnsignup_Click);
            // 
            // btnback
            // 
            this.btnback.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnback.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnback.Location = new System.Drawing.Point(422, 434);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(107, 49);
            this.btnback.TabIndex = 12;
            this.btnback.Text = "Back";
            this.btnback.UseVisualStyleBackColor = false;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // textBoxmail
            // 
            this.textBoxmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxmail.Location = new System.Drawing.Point(369, 374);
            this.textBoxmail.Name = "textBoxmail";
            this.textBoxmail.Size = new System.Drawing.Size(241, 34);
            this.textBoxmail.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(132, 374);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 37);
            this.label1.TabIndex = 13;
            this.label1.Text = "Email";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(321, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(162, 102);
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.BackgroundImage = global::Login.Properties.Resources.on_the_table_at_library_blurred_background_h0rouef1g_thumbnail_full01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(687, 536);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBoxmail);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnback);
            this.Controls.Add(this.btnsignup);
            this.Controls.Add(this.textBoxadd);
            this.Controls.Add(this.labeladd);
            this.Controls.Add(this.textBoxphn);
            this.Controls.Add(this.textBoxname);
            this.Controls.Add(this.textBoxpass);
            this.Controls.Add(this.textBoxid);
            this.Controls.Add(this.labelphn);
            this.Controls.Add(this.labelname);
            this.Controls.Add(this.labelpasswrd);
            this.Controls.Add(this.labelid);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Signup Customer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form3_FormClosing);
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelid;
        private System.Windows.Forms.Label labelpasswrd;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Label labelphn;
        private System.Windows.Forms.TextBox textBoxid;
        private System.Windows.Forms.TextBox textBoxpass;
        private System.Windows.Forms.TextBox textBoxname;
        private System.Windows.Forms.TextBox textBoxphn;
        private System.Windows.Forms.Label labeladd;
        private System.Windows.Forms.TextBox textBoxadd;
        private System.Windows.Forms.Button btnsignup;
        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.TextBox textBoxmail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}