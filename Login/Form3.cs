﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace Login
{
    public partial class Form3 : Form
    {
        Form1 f1;
        public Form3(Form1 f1)
        {
            this.f1 = f1;
            this.WindowState = f1.WindowState;
            InitializeComponent();
        }
        private void Form3_Load(object sender, EventArgs e)
        {
            Random x = new Random();
            string s = "C" + x.Next(999).ToString();
            textBoxid.Text = s;
        }
        string conString = "Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True";
        private void btnsignup_Click(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (!textBoxname.Text.ToString().Equals("") && !textBoxpass.Text.ToString().Equals("") && !textBoxphn.Text.ToString().Equals("") && !textBoxadd.Text.ToString().Equals("")||!textBoxmail.Text.ToString().Equals(""))
            {
                try
                {
                    int i = int.Parse(textBoxphn.Text.ToString());
                    //MessageBox.Show("Save Data");
                    bool j = int.TryParse(textBoxname.Text.ToString(), out i);
                    //MessageBox.Show("Save Data");
                    bool k = int.TryParse(textBoxadd.Text.ToString(), out i);
                    //MessageBox.Show("Save Data");
                    bool o = int.TryParse(textBoxname.Text.ToString().Substring(0, 1), out i);
                    if (textBoxphn.Text.ToString().Length == 11 && i >= 0)
                    {
                        if (j == false)
                        {
                            if (k == false)
                            {
                                if (o == false)
                                {
                                    if (MessageBox.Show("Are you sure?", "Sure", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        SmtpClient clientDetais = new SmtpClient();
                                        clientDetais.Port = 587;
                                        clientDetais.Host = "smtp.gmail.com";
                                        clientDetais.EnableSsl = true;
                                        clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                                        clientDetais.UseDefaultCredentials = false;
                                        clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");
                                        
                                        MailMessage maildetails = new MailMessage();
                                        maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                                        maildetails.To.Add(textBoxmail.Text.ToString());
                                        maildetails.Subject = "Account Created";
                                        maildetails.Body = "Welcome to our library "+textBoxname.Text.ToString()+"\n Your customer Id:"+textBoxid.Text.ToString();
                                        clientDetais.Send(maildetails);

                                        string query2 = "insert into Common values('" + textBoxid.Text.ToString() + "','" + "Customer" + "')";
                                        SqlCommand cmd = new SqlCommand(query2, sql);
                                        cmd.ExecuteNonQuery();
                                        string query = "insert into login values('" + textBoxid.Text.ToString() + "','" + textBoxpass.Text.ToString() + "','" + "Customer" + "','" + "Inactive" + "')";
                                        cmd = new SqlCommand(query, sql);
                                        cmd.ExecuteNonQuery();
                                        string query1 = "insert into CustomerData values('" + textBoxid.Text.ToString() + "','" + textBoxname.Text.ToString() + "','" + textBoxadd.Text.ToString() + "','" + textBoxphn.Text.ToString() + "','" + "Customer" + "','"+textBoxmail.Text.ToString()+"')";
                                        cmd = new SqlCommand(query1, sql);
                                        cmd.ExecuteNonQuery();
                                        MessageBox.Show("<<Success>>");
                                        this.Hide();
                                        f1.Show();
                                    }
                                    else
                                    {
 
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Invalid address");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid Name/Number Cannot be a name");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Phone Number is less then 11");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex.Message.ToString());
                }
            }
            else
            {
                MessageBox.Show("Invalid input or incomplete field");
            }
        }

        private void btnback_Click(object sender, EventArgs e)
        {
            this.Hide();
            f1.Show();
        }

        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
