﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace Login
{
    public partial class Form4 : Form
    {
        Form2 f;
        public string id;
        
        public Form4(Form2 f)
        {
            this.f = f;
            id = f.id;
            this.WindowState = f.WindowState;
            InitializeComponent();
        }
        string conString = "Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True";
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                try
                {
                    DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                    SqlConnection sql = new SqlConnection(conString);
                    sql.Open();
                    if (sql.State == ConnectionState.Open)
                    {
                        string query1 = "update login set Status ='" + row.Cells[3].Value.ToString() + "' where Id = '" + row.Cells["Id"].Value.ToString() + "'";
                        SqlCommand cmd = new SqlCommand(query1, sql);
                        MessageBox.Show("Save");
                        dataGridView1.Columns.RemoveAt(4);
                        dataGridView1.Columns.RemoveAt(3);
                        cmd.ExecuteNonQuery();
                        load();
                    }
                }
                catch (Exception )
                {
                    MessageBox.Show("Not Save");
                }

            }
            else { MessageBox.Show(e.ColumnIndex.ToString()); }

        }
        public void load()
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select l.Id,c.Name,l.Type,l.Status,c.Email from login l,CustomerData c where l.Id=c.Customer_Id";

                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.Fill;
                //DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();
                //cmb.HeaderText = "Status";
                //cmb.Name = "Combo";
                //cmb.Items.Add("Active");
                //cmb.Items.Add("Inactive");
                //dataGridView1.Columns.Add(cmb);

                //DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                //btn.HeaderText = "";
                //btn.Name = "button";
                //btn.Text = "Save";
                //btn.UseColumnTextForButtonValue = true;
                //dataGridView1.Columns.Add(btn);
            }
        }
        private void Form4_Load(object sender, EventArgs e)
        {
            load();
            
        }

        private void btnback_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f2 = new Form2(this);
            f2.Show();
        }

        private void Form4_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            if(comboBox1.Text.ToString().Equals("Active"))
            {
             SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select l.Id,c.Name,l.Type,l.Status,c.Email from login l,CustomerData c where l.Id=c.Customer_Id and l.Status ='Active'";

                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.Fill;
               
                
            }
            }
            else if (comboBox1.Text.ToString().Equals("Inactive"))
            {
                SqlConnection sql = new SqlConnection(conString);
                sql.Open();
                if (sql.State == ConnectionState.Open)
                {
                    string query = "select l.Id,c.Name,l.Type,l.Status,c.Email from login l,CustomerData c where l.Id=c.Customer_Id and l.Status ='Inactive'";

                    SqlCommand sql1 = new SqlCommand(query, sql);
                    SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    dataGridView1.DataSource = dt;
                    dataGridView1.AllowUserToAddRows = false;
                    dataGridView1.AllowUserToDeleteRows = false;
                    dataGridView1.AllowUserToResizeRows = false;
                    dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                    dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                    dataGridView1.AutoSizeColumnsMode =
                        DataGridViewAutoSizeColumnsMode.Fill;
                 
                }
 
            }
            else if (comboBox1.Text.ToString().Equals("All"))
            {
                SqlConnection sql = new SqlConnection(conString);
                sql.Open();
                if (sql.State == ConnectionState.Open)
                {
                    string query = "select l.Id,c.Name,l.Type,l.Status,c.Email from login l,CustomerData c where l.Id=c.Customer_Id";

                    SqlCommand sql1 = new SqlCommand(query, sql);
                    SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    dataGridView1.DataSource = dt;
                    dataGridView1.AllowUserToAddRows = false;
                    dataGridView1.AllowUserToDeleteRows = false;
                    dataGridView1.AllowUserToResizeRows = false;
                    dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                    dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                    dataGridView1.AutoSizeColumnsMode =
                        DataGridViewAutoSizeColumnsMode.Fill;

                }

            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string value=dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            if (value.Equals("Active"))
            {
                SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                SmtpClient clientDetais = new SmtpClient();
                clientDetais.Port = 587;
                clientDetais.Host = "smtp.gmail.com";
                clientDetais.EnableSsl = true;
                clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                clientDetais.UseDefaultCredentials = false;
                clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");
                MailMessage maildetails = new MailMessage();
                maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                maildetails.To.Add(dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString());
                maildetails.Subject = "Account BANNED";
                maildetails.Body = "Welcome to our library " + dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString() + "\n Dear Customer,\n You Have Been BANNED for some time\n CONTACT librarymanagementSystemAIUB@gmail.com  ";
                clientDetais.Send(maildetails);

                string query = "update login set Status='Inactive' where Id ='" + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "'";
                SqlCommand sql1 = new SqlCommand(query, sql);
                sql1.ExecuteNonQuery();
                MessageBox.Show("<<Success>>", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            }
            else if (value.Equals("Inactive"))
            {
                SqlConnection sql = new SqlConnection(conString);
                sql.Open();
                if (sql.State == ConnectionState.Open)
                {
                    SmtpClient clientDetais = new SmtpClient();
                    clientDetais.Port = 587;
                    clientDetais.Host = "smtp.gmail.com";
                    clientDetais.EnableSsl = true;
                    clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                    clientDetais.UseDefaultCredentials = false;
                    clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");
                    MailMessage maildetails = new MailMessage();
                    maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                    maildetails.To.Add(dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString());
                    maildetails.Subject = "Account Activated";
                    maildetails.Body = "Welcome to our library " + dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString() + "\n Your customer Id:" + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "\n YOU ARE NOW ACTIVATED\nYOU CAN USE OUR SYSTEM NOW";
                    clientDetais.Send(maildetails);

                    string query = "update login set Status='Active' where Id ='" + dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() + "'";
                    SqlCommand sql1 = new SqlCommand(query, sql);
                    sql1.ExecuteNonQuery();
                    MessageBox.Show("<<Success>>", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            SqlConnection sql2 = new SqlConnection(conString);
                sql2.Open();
                if (sql2.State == ConnectionState.Open)
                {
                    string query = "select l.Id,c.Name,l.Type,l.Status,c.Email from login l,CustomerData c where l.Id=c.Customer_Id";

                    SqlCommand sql1 = new SqlCommand(query, sql2);
                    SqlDataAdapter sda = new SqlDataAdapter(query, sql2);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    dataGridView1.DataSource = dt;
                    dataGridView1.AllowUserToAddRows = false;
                    dataGridView1.AllowUserToDeleteRows = false;
                    dataGridView1.AllowUserToResizeRows = false;
                    dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                    dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                    dataGridView1.AutoSizeColumnsMode =
                        DataGridViewAutoSizeColumnsMode.Fill;
                    comboBox1.Text = "";
                }
            MessageBox.Show(value);
        }
        
    }
}
