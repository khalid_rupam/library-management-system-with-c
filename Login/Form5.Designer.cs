﻿namespace Login
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnback = new System.Windows.Forms.Button();
            this.btnsignup = new System.Windows.Forms.Button();
            this.TxtType = new System.Windows.Forms.TextBox();
            this.labeladd = new System.Windows.Forms.Label();
            this.txtAdd = new System.Windows.Forms.TextBox();
            this.txtPhn = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.textBoxid = new System.Windows.Forms.TextBox();
            this.labelphn = new System.Windows.Forms.Label();
            this.labelname = new System.Windows.Forms.Label();
            this.labelpasswrd = new System.Windows.Forms.Label();
            this.labelid = new System.Windows.Forms.Label();
            this.txtsal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnback
            // 
            this.btnback.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnback.Location = new System.Drawing.Point(453, 380);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(107, 49);
            this.btnback.TabIndex = 24;
            this.btnback.Text = "Back";
            this.btnback.UseVisualStyleBackColor = false;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // btnsignup
            // 
            this.btnsignup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnsignup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnsignup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsignup.Location = new System.Drawing.Point(202, 380);
            this.btnsignup.Name = "btnsignup";
            this.btnsignup.Size = new System.Drawing.Size(107, 49);
            this.btnsignup.TabIndex = 23;
            this.btnsignup.Text = "update";
            this.btnsignup.UseVisualStyleBackColor = false;
            this.btnsignup.Click += new System.EventHandler(this.btnsignup_Click);
            // 
            // TxtType
            // 
            this.TxtType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TxtType.Enabled = false;
            this.TxtType.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtType.Location = new System.Drawing.Point(390, 258);
            this.TxtType.Name = "TxtType";
            this.TxtType.Size = new System.Drawing.Size(241, 34);
            this.TxtType.TabIndex = 22;
            // 
            // labeladd
            // 
            this.labeladd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labeladd.AutoSize = true;
            this.labeladd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeladd.Location = new System.Drawing.Point(144, 258);
            this.labeladd.Name = "labeladd";
            this.labeladd.Size = new System.Drawing.Size(68, 29);
            this.labeladd.TabIndex = 21;
            this.labeladd.Text = "Type";
            // 
            // txtAdd
            // 
            this.txtAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdd.Location = new System.Drawing.Point(390, 209);
            this.txtAdd.Name = "txtAdd";
            this.txtAdd.Size = new System.Drawing.Size(241, 34);
            this.txtAdd.TabIndex = 20;
            // 
            // txtPhn
            // 
            this.txtPhn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtPhn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhn.Location = new System.Drawing.Point(390, 162);
            this.txtPhn.Name = "txtPhn";
            this.txtPhn.Size = new System.Drawing.Size(241, 34);
            this.txtPhn.TabIndex = 19;
            // 
            // txtName
            // 
            this.txtName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(390, 110);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(241, 34);
            this.txtName.TabIndex = 18;
            // 
            // textBoxid
            // 
            this.textBoxid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxid.Enabled = false;
            this.textBoxid.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxid.Location = new System.Drawing.Point(390, 56);
            this.textBoxid.Name = "textBoxid";
            this.textBoxid.Size = new System.Drawing.Size(241, 34);
            this.textBoxid.TabIndex = 17;
            // 
            // labelphn
            // 
            this.labelphn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelphn.AutoSize = true;
            this.labelphn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelphn.Location = new System.Drawing.Point(144, 202);
            this.labelphn.Name = "labelphn";
            this.labelphn.Size = new System.Drawing.Size(102, 29);
            this.labelphn.TabIndex = 16;
            this.labelphn.Text = "Address";
            // 
            // labelname
            // 
            this.labelname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelname.AutoSize = true;
            this.labelname.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelname.Location = new System.Drawing.Point(144, 152);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(176, 29);
            this.labelname.TabIndex = 15;
            this.labelname.Text = "Phone Number";
            // 
            // labelpasswrd
            // 
            this.labelpasswrd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelpasswrd.AutoSize = true;
            this.labelpasswrd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelpasswrd.Location = new System.Drawing.Point(144, 103);
            this.labelpasswrd.Name = "labelpasswrd";
            this.labelpasswrd.Size = new System.Drawing.Size(78, 29);
            this.labelpasswrd.TabIndex = 14;
            this.labelpasswrd.Text = "Name";
            // 
            // labelid
            // 
            this.labelid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelid.AutoSize = true;
            this.labelid.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelid.Location = new System.Drawing.Point(144, 56);
            this.labelid.Name = "labelid";
            this.labelid.Size = new System.Drawing.Size(148, 29);
            this.labelid.TabIndex = 13;
            this.labelid.Text = "Employee Id";
            // 
            // txtsal
            // 
            this.txtsal.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtsal.Enabled = false;
            this.txtsal.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsal.Location = new System.Drawing.Point(390, 315);
            this.txtsal.Name = "txtsal";
            this.txtsal.Size = new System.Drawing.Size(241, 34);
            this.txtsal.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(144, 315);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 29);
            this.label1.TabIndex = 25;
            this.label1.Text = "Salary";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.groupBox1.Controls.Add(this.labelid);
            this.groupBox1.Controls.Add(this.txtsal);
            this.groupBox1.Controls.Add(this.labelpasswrd);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labelname);
            this.groupBox1.Controls.Add(this.btnback);
            this.groupBox1.Controls.Add(this.labelphn);
            this.groupBox1.Controls.Add(this.btnsignup);
            this.groupBox1.Controls.Add(this.textBoxid);
            this.groupBox1.Controls.Add(this.TxtType);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.labeladd);
            this.groupBox1.Controls.Add(this.txtPhn);
            this.groupBox1.Controls.Add(this.txtAdd);
            this.groupBox1.Location = new System.Drawing.Point(146, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(726, 458);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Information";
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.BackgroundImage = global::Login.Properties.Resources.on_the_table_at_library_blurred_background_h0rouef1g_thumbnail_full01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(995, 536);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form5";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Info";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form5_FormClosing);
            this.Load += new System.EventHandler(this.Form5_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Button btnsignup;
        private System.Windows.Forms.TextBox TxtType;
        private System.Windows.Forms.Label labeladd;
        private System.Windows.Forms.TextBox txtAdd;
        private System.Windows.Forms.TextBox txtPhn;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox textBoxid;
        private System.Windows.Forms.Label labelphn;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Label labelpasswrd;
        private System.Windows.Forms.Label labelid;
        private System.Windows.Forms.TextBox txtsal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;

    }
}