﻿namespace Login
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddemp = new System.Windows.Forms.Button();
            this.btnempdetails = new System.Windows.Forms.Button();
            this.btnbanned = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAddemp
            // 
            this.btnAddemp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddemp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnAddemp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddemp.Location = new System.Drawing.Point(432, 127);
            this.btnAddemp.Name = "btnAddemp";
            this.btnAddemp.Size = new System.Drawing.Size(134, 45);
            this.btnAddemp.TabIndex = 0;
            this.btnAddemp.Text = "Add Employee";
            this.btnAddemp.UseVisualStyleBackColor = false;
            this.btnAddemp.Click += new System.EventHandler(this.btnAddemp_Click);
            // 
            // btnempdetails
            // 
            this.btnempdetails.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnempdetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnempdetails.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnempdetails.Location = new System.Drawing.Point(432, 217);
            this.btnempdetails.Name = "btnempdetails";
            this.btnempdetails.Size = new System.Drawing.Size(134, 45);
            this.btnempdetails.TabIndex = 1;
            this.btnempdetails.Text = "Employee Details";
            this.btnempdetails.UseVisualStyleBackColor = false;
            this.btnempdetails.Click += new System.EventHandler(this.btnempdetails_Click);
            // 
            // btnbanned
            // 
            this.btnbanned.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnbanned.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnbanned.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnbanned.Location = new System.Drawing.Point(432, 298);
            this.btnbanned.Name = "btnbanned";
            this.btnbanned.Size = new System.Drawing.Size(134, 45);
            this.btnbanned.TabIndex = 2;
            this.btnbanned.Text = "BANNED EMPLOYEE";
            this.btnbanned.UseVisualStyleBackColor = false;
            this.btnbanned.Click += new System.EventHandler(this.btnbanned_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Location = new System.Drawing.Point(432, 376);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(134, 45);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Login.Properties.Resources.on_the_table_at_library_blurred_background_h0rouef1g_thumbnail_full01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(995, 536);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnbanned);
            this.Controls.Add(this.btnempdetails);
            this.Controls.Add(this.btnAddemp);
            this.Name = "Form6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage Employee";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form6_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddemp;
        private System.Windows.Forms.Button btnempdetails;
        private System.Windows.Forms.Button btnbanned;
        private System.Windows.Forms.Button btnBack;
    }
}