﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login
{
    public partial class Form6 : Form
    {
        Form2 f1;
        Form7 f2;
        Form10 f10;
        Form11 f11;

        public string id;
        public Form6(object f)
        {
            if (f is Form2)
            {
                this.f1 = (Form2)f;
                this.id = f1.id;
                this.WindowState = f1.WindowState;
            }
            else if (f is Form7)
            {
                this.f2 = (Form7)f;
                id = f2.id;
                this.WindowState = f2.WindowState;
            }
            else if (f is Form10)
            {
                this.f10 = (Form10)f;
                this.id = f10.id;
                this.WindowState = f10.WindowState;
            }
            else if (f is Form11)
            {
                this.f11 = (Form11)f;
                this.id = f11.id;
                this.WindowState = f11.WindowState;
            }
            InitializeComponent();
        }

        private void btnAddemp_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form7 f7 = new Form7(this);
            f7.Show();
        }

        private void Form6_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f = new Form2(this);
            f.Show();
        }

        private void btnempdetails_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form10 f10 = new Form10(this);
            f10.Show();

        }

        private void btnbanned_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form11 f11 = new Form11(this);
            f11.Show();

        }
    }
}
