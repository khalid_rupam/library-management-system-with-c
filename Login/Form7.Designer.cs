﻿namespace Login
{
    partial class Form7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnback = new System.Windows.Forms.Button();
            this.btnsignup = new System.Windows.Forms.Button();
            this.textBoxadd = new System.Windows.Forms.TextBox();
            this.labeladd = new System.Windows.Forms.Label();
            this.textBoxphn = new System.Windows.Forms.TextBox();
            this.textBoxname = new System.Windows.Forms.TextBox();
            this.textBoxpass = new System.Windows.Forms.TextBox();
            this.textBoxid = new System.Windows.Forms.TextBox();
            this.labelphn = new System.Windows.Forms.Label();
            this.labelname = new System.Windows.Forms.Label();
            this.labelpasswrd = new System.Windows.Forms.Label();
            this.labelid = new System.Windows.Forms.Label();
            this.textBoxSal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxmail = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnback
            // 
            this.btnback.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnback.Location = new System.Drawing.Point(439, 457);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(107, 49);
            this.btnback.TabIndex = 25;
            this.btnback.Text = "Back";
            this.btnback.UseVisualStyleBackColor = false;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // btnsignup
            // 
            this.btnsignup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnsignup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnsignup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsignup.Location = new System.Drawing.Point(188, 457);
            this.btnsignup.Name = "btnsignup";
            this.btnsignup.Size = new System.Drawing.Size(107, 49);
            this.btnsignup.TabIndex = 24;
            this.btnsignup.Text = "sign up";
            this.btnsignup.UseVisualStyleBackColor = false;
            this.btnsignup.Click += new System.EventHandler(this.btnsignup_Click);
            // 
            // textBoxadd
            // 
            this.textBoxadd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxadd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxadd.Location = new System.Drawing.Point(372, 247);
            this.textBoxadd.Name = "textBoxadd";
            this.textBoxadd.Size = new System.Drawing.Size(241, 34);
            this.textBoxadd.TabIndex = 22;
            // 
            // labeladd
            // 
            this.labeladd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labeladd.AutoSize = true;
            this.labeladd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeladd.Location = new System.Drawing.Point(126, 247);
            this.labeladd.Name = "labeladd";
            this.labeladd.Size = new System.Drawing.Size(114, 29);
            this.labeladd.TabIndex = 21;
            this.labeladd.Text = "Address :";
            // 
            // textBoxphn
            // 
            this.textBoxphn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxphn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxphn.Location = new System.Drawing.Point(372, 198);
            this.textBoxphn.Name = "textBoxphn";
            this.textBoxphn.Size = new System.Drawing.Size(241, 34);
            this.textBoxphn.TabIndex = 20;
            // 
            // textBoxname
            // 
            this.textBoxname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxname.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxname.Location = new System.Drawing.Point(372, 151);
            this.textBoxname.Name = "textBoxname";
            this.textBoxname.Size = new System.Drawing.Size(241, 34);
            this.textBoxname.TabIndex = 19;
            // 
            // textBoxpass
            // 
            this.textBoxpass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxpass.Enabled = false;
            this.textBoxpass.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxpass.Location = new System.Drawing.Point(372, 99);
            this.textBoxpass.Name = "textBoxpass";
            this.textBoxpass.Size = new System.Drawing.Size(241, 34);
            this.textBoxpass.TabIndex = 18;
            // 
            // textBoxid
            // 
            this.textBoxid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxid.Enabled = false;
            this.textBoxid.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxid.Location = new System.Drawing.Point(372, 45);
            this.textBoxid.Name = "textBoxid";
            this.textBoxid.Size = new System.Drawing.Size(241, 34);
            this.textBoxid.TabIndex = 17;
            // 
            // labelphn
            // 
            this.labelphn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelphn.AutoSize = true;
            this.labelphn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelphn.Location = new System.Drawing.Point(126, 191);
            this.labelphn.Name = "labelphn";
            this.labelphn.Size = new System.Drawing.Size(188, 29);
            this.labelphn.TabIndex = 16;
            this.labelphn.Text = "Phone Number :";
            // 
            // labelname
            // 
            this.labelname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelname.AutoSize = true;
            this.labelname.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelname.Location = new System.Drawing.Point(126, 141);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(90, 29);
            this.labelname.TabIndex = 15;
            this.labelname.Text = "Name :";
            // 
            // labelpasswrd
            // 
            this.labelpasswrd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelpasswrd.AutoSize = true;
            this.labelpasswrd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelpasswrd.Location = new System.Drawing.Point(126, 92);
            this.labelpasswrd.Name = "labelpasswrd";
            this.labelpasswrd.Size = new System.Drawing.Size(132, 29);
            this.labelpasswrd.TabIndex = 14;
            this.labelpasswrd.Text = "Password :";
            // 
            // labelid
            // 
            this.labelid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelid.AutoSize = true;
            this.labelid.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelid.Location = new System.Drawing.Point(126, 45);
            this.labelid.Name = "labelid";
            this.labelid.Size = new System.Drawing.Size(102, 29);
            this.labelid.TabIndex = 13;
            this.labelid.Text = "User Id :";
            // 
            // textBoxSal
            // 
            this.textBoxSal.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxSal.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSal.Location = new System.Drawing.Point(372, 344);
            this.textBoxSal.Name = "textBoxSal";
            this.textBoxSal.Size = new System.Drawing.Size(241, 34);
            this.textBoxSal.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(126, 298);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 29);
            this.label1.TabIndex = 26;
            this.label1.Text = "Type :";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(126, 347);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 29);
            this.label2.TabIndex = 28;
            this.label2.Text = "Salary :";
            // 
            // comboBoxType
            // 
            this.comboBoxType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxType.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "Manager",
            "General"});
            this.comboBoxType.Location = new System.Drawing.Point(372, 295);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(241, 37);
            this.comboBoxType.TabIndex = 29;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBoxmail);
            this.groupBox1.Controls.Add(this.comboBoxType);
            this.groupBox1.Controls.Add(this.labelid);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.labelpasswrd);
            this.groupBox1.Controls.Add(this.textBoxSal);
            this.groupBox1.Controls.Add(this.labelname);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labelphn);
            this.groupBox1.Controls.Add(this.btnback);
            this.groupBox1.Controls.Add(this.textBoxid);
            this.groupBox1.Controls.Add(this.btnsignup);
            this.groupBox1.Controls.Add(this.textBoxpass);
            this.groupBox1.Controls.Add(this.textBoxname);
            this.groupBox1.Controls.Add(this.textBoxadd);
            this.groupBox1.Controls.Add(this.textBoxphn);
            this.groupBox1.Controls.Add(this.labeladd);
            this.groupBox1.Location = new System.Drawing.Point(153, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(739, 524);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Fill up form";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(126, 397);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 29);
            this.label3.TabIndex = 31;
            this.label3.Text = "Mail";
            // 
            // textBoxmail
            // 
            this.textBoxmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxmail.Location = new System.Drawing.Point(372, 394);
            this.textBoxmail.Name = "textBoxmail";
            this.textBoxmail.Size = new System.Drawing.Size(241, 34);
            this.textBoxmail.TabIndex = 30;
            // 
            // Form7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.BackgroundImage = global::Login.Properties.Resources.on_the_table_at_library_blurred_background_h0rouef1g_thumbnail_full01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(995, 559);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form7";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Employee";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form7_FormClosing);
            this.Load += new System.EventHandler(this.Form7_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Button btnsignup;
        private System.Windows.Forms.TextBox textBoxadd;
        private System.Windows.Forms.Label labeladd;
        private System.Windows.Forms.TextBox textBoxphn;
        private System.Windows.Forms.TextBox textBoxname;
        private System.Windows.Forms.TextBox textBoxpass;
        private System.Windows.Forms.TextBox textBoxid;
        private System.Windows.Forms.Label labelphn;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Label labelpasswrd;
        private System.Windows.Forms.Label labelid;
        private System.Windows.Forms.TextBox textBoxSal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxmail;
    }
}