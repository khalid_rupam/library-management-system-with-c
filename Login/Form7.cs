﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace Login
{
    public partial class Form7 : Form
    {
        Form6 f;
        public string id;
        public Form7(Form6 f)
        {
            this.f = f;
            id = f.id;
            this.WindowState = f.WindowState;
            InitializeComponent();
        }

        private void Form7_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            Random x = new Random();
            string s = "E" + x.Next(999).ToString();
            textBoxid.Text = s;
            string l = x.Next(999999).ToString();
            textBoxpass.Text = l;
        }
        string conString = "Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True";
        private void btnsignup_Click(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (!textBoxname.Text.ToString().Equals("") && !textBoxpass.Text.ToString().Equals("") && !textBoxphn.Text.ToString().Equals("") && !textBoxadd.Text.ToString().Equals("")&&!textBoxSal.Text.ToString().Equals("")&&!comboBoxType.Text.ToString().Equals(""))
            {
                try
                {
                    int i = int.Parse(textBoxphn.Text.ToString());
                    //MessageBox.Show("Save Data");
                    bool j = int.TryParse(textBoxname.Text.ToString(), out i);
                    //MessageBox.Show("Save Data");
                    bool k = int.TryParse(textBoxadd.Text.ToString(), out i);
                    //MessageBox.Show("Save Data");
                    bool o = int.TryParse(textBoxname.Text.ToString().Substring(0, 1), out i);
                    string r = textBoxphn.Text.ToString().Substring(0, 3);
                    if (textBoxphn.Text.ToString().Length == 11 && i >= 0)
                    {
                        if (r.Equals("017") || r.Equals("019") || r.Equals("015") || r.Equals("018"))
                        {
                            if (j == false)
                            {
                                if (k == false)
                                {
                                    if (o == false)
                                    {
                                        if (MessageBox.Show("Are you sure?", "Sure", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                                        {
                                            SmtpClient clientDetais = new SmtpClient();
                                            clientDetais.Port = 587;
                                            clientDetais.Host = "smtp.gmail.com";
                                            clientDetais.EnableSsl = true;
                                            clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                                            clientDetais.UseDefaultCredentials = false;
                                            clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");
                                            MailMessage maildetails = new MailMessage();
                                            maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                                            maildetails.To.Add(textBoxmail.Text.ToString());
                                            maildetails.Subject = "Account Created";
                                            maildetails.Body = "Welcome to our library " + textBoxname.Text.ToString() + "\n Your Employee Id:" + textBoxid.Text.ToString()+"\n Your Password : "+textBoxpass.Text.ToString();
                                             

                                            string query2 = "insert into Common values('" + textBoxid.Text.ToString() + "','" + comboBoxType.Text.ToString() + "')";
                                            SqlCommand cmd = new SqlCommand(query2, sql);
                                            cmd.ExecuteNonQuery();
                                            string query = "insert into login values('" + textBoxid.Text.ToString() + "','" + textBoxpass.Text.ToString() + "','" + comboBoxType.Text.ToString() + "','" + "Active" + "')";
                                            cmd = new SqlCommand(query, sql);
                                            cmd.ExecuteNonQuery();
                                            string query1 = "insert into Employee values('" + textBoxid.Text.ToString() + "','" + textBoxname.Text.ToString() + "','" + textBoxphn.Text.ToString() + "','" + textBoxadd.Text.ToString() + "','" + comboBoxType.Text.ToString() + "','" + textBoxSal.Text.ToString() + "','"+textBoxmail.Text.ToString()+"')";
                                            cmd = new SqlCommand(query1, sql);
                                            cmd.ExecuteNonQuery();
                                            clientDetais.Send(maildetails);
                                            MessageBox.Show("<<Success>>");
                                            this.Hide();
                                            Form6 f6 = new Form6(this);
                                            f6.Show();

                                        }
                                        else
                                        {

                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Invalid address");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Invalid Name/Number Cannot be a name");
                            }
                        }
                        else
                        {
                            MessageBox.Show("invalid operator");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Phone Number is less then 11");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex.Message.ToString());
                }
            }
            else
            {
                MessageBox.Show("Invalid input or incomplete field");
            }
        }

        private void btnback_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form6 f6 = new Form6(this);
            f6.Show();
        }
    }
}
