﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Login
{
    public partial class Form8 : Form
    {
        Form2 f1;
        public string id;
        public Form8(Form2 f)
        {
            this.f1 = f;
            id = f1.id;
            this.WindowState = f1.WindowState;
            InitializeComponent();
        }
        string conString = "Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True";
        public void load()
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select * from book";
                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =DataGridViewAutoSizeColumnsMode.Fill;
            }
        }
        private void Form8_Load(object sender, EventArgs e)
        {
            load();
        }
        private void Form8_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            textBoxBookid.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBoxtitle.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBoxauthor.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBoxpublication.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            textBoxqnty.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f = new Form2(this);
            f.Show();
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                if (!textBoxBookid.Text.ToString().Equals(""))
                {
                    int a;
                    try
                    {
                        int t = int.Parse(textBoxqnty.Text.ToString());
                        bool b = int.TryParse(textBoxtitle.Text.ToString(), out a);
                        bool c = int.TryParse(textBoxauthor.Text.ToString(), out a);
                        bool d = int.TryParse(textBoxpublication.Text.ToString(), out a);
                        //MessageBox.Show(t.ToString());
                        if (t >= 0)
                        {
                            if (b == false)
                            {
                                if (c == false)
                                {
                                    if (d == true)
                                    {
                                        string query = "update book set bookTitle='" + textBoxtitle.Text.ToString() + "',authorName='" + textBoxauthor.Text.ToString() + "',publicationYear='" + textBoxpublication.Text.ToString() + "',availableQuantity='" + textBoxqnty.Text.ToString() + "' where bookId ='"+textBoxBookid.Text.ToString()+"'";
                                        
                                        SqlCommand sql1 = new SqlCommand(query, sql);
                                        sql1.ExecuteNonQuery();
                                        MessageBox.Show("<<Success>>", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                        load(); 
                                    }
                                    else
                                    {
                                        MessageBox.Show("Year can not be a character");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Name can not be number");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Title error");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Quantity Can not be negative");
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("invalid Quantity");
                    }
                }
                else
                {
                    MessageBox.Show("No Data Found");
                }
            }

            else
            {
                MessageBox.Show("Not ok");
            }
        }
        private void BtnRefrash_Click(object sender, EventArgs e)
        {
            textBoxBookid.Text = "";
            textBoxtitle.Text = "";
            textBoxauthor.Text = "";
            textBoxpublication.Text = "";
            textBoxqnty.Text = "";
            load();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);

            sql.Open();

            if (sql.State == ConnectionState.Open)
            {
                if (!textBoxBookid.Text.ToString().Equals(""))
                {
                    string query = "delete from book where bookId ='" + textBoxBookid.Text.ToString() + "'";
                    SqlCommand sql1 = new SqlCommand(query, sql);
                    sql1.ExecuteNonQuery();
                    textBoxBookid.Text = "";
                    textBoxtitle.Text = "";
                    textBoxauthor.Text = "";
                    textBoxpublication.Text = "";
                    textBoxqnty.Text = "";
                    load();
                    MessageBox.Show("<<SUCCESS>>", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show("No Data Found");
                }
            }
            else
            {
                MessageBox.Show("Not ok");
            }
        }
        private void textBoxsearch_TextChanged(object sender, EventArgs e)
        {
            string value = textBoxsearch.Text.ToString();
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select * from book where bookTitle like '" + value + "%'";
                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.Fill;

            }
        }
    }
}
