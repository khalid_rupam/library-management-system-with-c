﻿namespace Login
{
    partial class Form9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnback = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.Fillup = new System.Windows.Forms.Label();
            this.textBoxavlqnty = new System.Windows.Forms.TextBox();
            this.labeladd = new System.Windows.Forms.Label();
            this.textBoxpublicyear = new System.Windows.Forms.TextBox();
            this.textBoxauthor = new System.Windows.Forms.TextBox();
            this.textBoxBooktle = new System.Windows.Forms.TextBox();
            this.textBookid = new System.Windows.Forms.TextBox();
            this.labelphn = new System.Windows.Forms.Label();
            this.labelname = new System.Windows.Forms.Label();
            this.labelpasswrd = new System.Windows.Forms.Label();
            this.labelid = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnback
            // 
            this.btnback.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnback.Location = new System.Drawing.Point(362, 378);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(107, 49);
            this.btnback.TabIndex = 25;
            this.btnback.Text = "Back";
            this.btnback.UseVisualStyleBackColor = false;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdd.Location = new System.Drawing.Point(111, 378);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(107, 49);
            this.btnAdd.TabIndex = 24;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // Fillup
            // 
            this.Fillup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Fillup.AutoSize = true;
            this.Fillup.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fillup.Location = new System.Drawing.Point(141, 18);
            this.Fillup.Name = "Fillup";
            this.Fillup.Size = new System.Drawing.Size(299, 29);
            this.Fillup.TabIndex = 23;
            this.Fillup.Text = "Fill Up The Following Data";
            // 
            // textBoxavlqnty
            // 
            this.textBoxavlqnty.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxavlqnty.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxavlqnty.Location = new System.Drawing.Point(300, 283);
            this.textBoxavlqnty.Name = "textBoxavlqnty";
            this.textBoxavlqnty.Size = new System.Drawing.Size(241, 34);
            this.textBoxavlqnty.TabIndex = 22;
            // 
            // labeladd
            // 
            this.labeladd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labeladd.AutoSize = true;
            this.labeladd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeladd.Location = new System.Drawing.Point(54, 283);
            this.labeladd.Name = "labeladd";
            this.labeladd.Size = new System.Drawing.Size(204, 29);
            this.labeladd.TabIndex = 21;
            this.labeladd.Text = "Available Quantity";
            // 
            // textBoxpublicyear
            // 
            this.textBoxpublicyear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxpublicyear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxpublicyear.Location = new System.Drawing.Point(300, 234);
            this.textBoxpublicyear.Name = "textBoxpublicyear";
            this.textBoxpublicyear.Size = new System.Drawing.Size(241, 34);
            this.textBoxpublicyear.TabIndex = 20;
            // 
            // textBoxauthor
            // 
            this.textBoxauthor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxauthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxauthor.Location = new System.Drawing.Point(300, 187);
            this.textBoxauthor.Name = "textBoxauthor";
            this.textBoxauthor.Size = new System.Drawing.Size(241, 34);
            this.textBoxauthor.TabIndex = 19;
            // 
            // textBoxBooktle
            // 
            this.textBoxBooktle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxBooktle.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBooktle.Location = new System.Drawing.Point(300, 135);
            this.textBoxBooktle.Name = "textBoxBooktle";
            this.textBoxBooktle.Size = new System.Drawing.Size(241, 34);
            this.textBoxBooktle.TabIndex = 18;
            // 
            // textBookid
            // 
            this.textBookid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBookid.Enabled = false;
            this.textBookid.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBookid.Location = new System.Drawing.Point(300, 81);
            this.textBookid.Name = "textBookid";
            this.textBookid.Size = new System.Drawing.Size(241, 34);
            this.textBookid.TabIndex = 17;
            // 
            // labelphn
            // 
            this.labelphn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelphn.AutoSize = true;
            this.labelphn.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelphn.Location = new System.Drawing.Point(54, 227);
            this.labelphn.Name = "labelphn";
            this.labelphn.Size = new System.Drawing.Size(189, 29);
            this.labelphn.TabIndex = 16;
            this.labelphn.Text = "Publication Year";
            // 
            // labelname
            // 
            this.labelname.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelname.AutoSize = true;
            this.labelname.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelname.Location = new System.Drawing.Point(54, 177);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(153, 29);
            this.labelname.TabIndex = 15;
            this.labelname.Text = "Author Name";
            // 
            // labelpasswrd
            // 
            this.labelpasswrd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelpasswrd.AutoSize = true;
            this.labelpasswrd.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelpasswrd.Location = new System.Drawing.Point(54, 128);
            this.labelpasswrd.Name = "labelpasswrd";
            this.labelpasswrd.Size = new System.Drawing.Size(123, 29);
            this.labelpasswrd.TabIndex = 14;
            this.labelpasswrd.Text = "Book Title";
            // 
            // labelid
            // 
            this.labelid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelid.AutoSize = true;
            this.labelid.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelid.Location = new System.Drawing.Point(54, 81);
            this.labelid.Name = "labelid";
            this.labelid.Size = new System.Drawing.Size(95, 29);
            this.labelid.TabIndex = 13;
            this.labelid.Text = "Book Id";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.groupBox1.Controls.Add(this.Fillup);
            this.groupBox1.Controls.Add(this.btnback);
            this.groupBox1.Controls.Add(this.labelid);
            this.groupBox1.Controls.Add(this.btnAdd);
            this.groupBox1.Controls.Add(this.labelpasswrd);
            this.groupBox1.Controls.Add(this.labelname);
            this.groupBox1.Controls.Add(this.textBoxavlqnty);
            this.groupBox1.Controls.Add(this.labelphn);
            this.groupBox1.Controls.Add(this.labeladd);
            this.groupBox1.Controls.Add(this.textBookid);
            this.groupBox1.Controls.Add(this.textBoxpublicyear);
            this.groupBox1.Controls.Add(this.textBoxBooktle);
            this.groupBox1.Controls.Add(this.textBoxauthor);
            this.groupBox1.Location = new System.Drawing.Point(217, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(592, 447);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add Book";
            // 
            // Form9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Login.Properties.Resources.on_the_table_at_library_blurred_background_h0rouef1g_thumbnail_full01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(995, 536);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form9";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ADD BOOK";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form9_FormClosing);
            this.Load += new System.EventHandler(this.Form9_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label Fillup;
        private System.Windows.Forms.TextBox textBoxavlqnty;
        private System.Windows.Forms.Label labeladd;
        private System.Windows.Forms.TextBox textBoxpublicyear;
        private System.Windows.Forms.TextBox textBoxauthor;
        private System.Windows.Forms.TextBox textBoxBooktle;
        private System.Windows.Forms.TextBox textBookid;
        private System.Windows.Forms.Label labelphn;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Label labelpasswrd;
        private System.Windows.Forms.Label labelid;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}