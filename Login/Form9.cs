﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Login
{
    public partial class Form9 : Form
    {
        Form2 f2;
        public string id;
        public Form9(Form2 f2)
        {
            this.f2 = f2;
            this.id = f2.id;
            this.WindowState = f2.WindowState;
            InitializeComponent();
        }

        private void Form9_Load(object sender, EventArgs e)
        {
            Random x = new Random();
            string s = "B" + x.Next(999).ToString();
            textBookid.Text = s;
        }

        private void Form9_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        string conString = "Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True";
        private void btnAdd_Click(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (!textBoxBooktle.Text.ToString().Equals("") && !textBoxauthor.Text.ToString().Equals("") && !textBoxpublicyear.Text.ToString().Equals("") && !textBoxavlqnty.Text.ToString().Equals(""))
            {
                try
                {
                    int a;
                    int t = int.Parse(textBoxavlqnty.Text.ToString());
                    bool b = int.TryParse(textBoxBooktle.Text.ToString(), out a);
                    bool c = int.TryParse(textBoxauthor.Text.ToString(), out a);
                    bool d = int.TryParse(textBoxpublicyear.Text.ToString(), out a);
                    if (t > 0)
                    {
                        if (b == false)
                        {
                            if (c == false)
                            {
                                if (d == true)
                                {
                                    if (MessageBox.Show("Are you sure?", "Sure", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        string query = "insert into book values('" + textBookid.Text.ToString() + "','" + textBoxBooktle.Text.ToString() + "','" + textBoxauthor.Text.ToString() + "','" + textBoxpublicyear.Text.ToString() + "','" + textBoxavlqnty.Text.ToString() + "')";
                                        SqlCommand cmd = new SqlCommand(query, sql);
                                        cmd.ExecuteNonQuery();
                                        MessageBox.Show("<<Success>>");
                                    }
                                    else
                                    {

                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Year Can not be charecter");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Invalid author name");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Book name invalid /n/Number Cannot be a name");
                        }
                    }
                    else
                    {
                        MessageBox.Show("year can not be negative");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex.Message.ToString());
                }
            }
            else
            {
                MessageBox.Show("Invalid input or incomplete field");
            }

        }

        private void btnback_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f2 = new Form2(this);
            f2.Show();
        }
    }
}
