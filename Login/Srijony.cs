﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Login
{
    public partial class Srijony : Form
    {
        Form2 f2;
        public string id;
        public Srijony( Form2 f2)
        {
            this.f2 = f2;
            this.id = f2.id;
            this.WindowState = f2.WindowState;
            InitializeComponent();
        }

        private void textBoxqnty_TextChanged(object sender, EventArgs e)
        {

        }
        string conString = "Data Source=DESKTOP-96SV9FH;Initial Catalog=project;Integrated Security=True";
        private void Srijony_Load(object sender, EventArgs e)
        {
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select * from CustomerData";

                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.Fill;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f = new Form2(this);
            f.Show();

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            textBoxcid.Text  = dataGridView1.Rows[e.RowIndex].Cells [0].Value.ToString();
            textBoxcname.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBoxcaddress.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBoxcphnnmbr.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            textBoxctype.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            textBoxmail.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (!textBoxcname.Text.ToString().Equals("") && !textBoxcaddress.Text.ToString().Equals("") && !textBoxcphnnmbr.Text.ToString().Equals(""))
            {
                try
                {
                    int i = int.Parse(textBoxcphnnmbr.Text.ToString());
                    //MessageBox.Show("Save Data");
                    bool j = int.TryParse(textBoxcname.Text.ToString(), out i);
                    //MessageBox.Show("Save Data");
                    bool o = int.TryParse(textBoxcname.ToString().Substring(0, 1), out i);
                    bool k = int.TryParse(textBoxcaddress.Text.ToString(), out i);
                    //MessageBox.Show(o.ToString());
                    //MessageBox.Show(txtName.Text.ToString().Substring(0, 1));
                    string r = textBoxcphnnmbr.Text.ToString().Substring(0, 3);
                    if (textBoxcphnnmbr.Text.ToString().Length == 11 && i >= 0)
                    {
                        if (r.Equals("017") || r.Equals("019") || r.Equals("015") || r.Equals("018"))
                        {
                            if (j == false)
                            {
                                if (k == false)
                                {
                                    if (o == false)
                                    {
                                        if (MessageBox.Show("Are you sure?", "Sure", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                                        {
                                            string query1 = "update CustomerData set Name = '" + textBoxcname.Text.ToString() + "',Address = '" + textBoxcaddress.Text.ToString() + "',PhoneNumber = '" + textBoxcphnnmbr.Text.ToString() + "',Email='"+textBoxmail.Text.ToString()+"' where Customer_Id = '" + textBoxcid.Text.ToString() + "'";
                                            SqlCommand cmd = new SqlCommand(query1, sql);
                                            cmd = new SqlCommand(query1, sql);
                                            cmd.ExecuteNonQuery();
                                            MessageBox.Show("<<Success>>");
                                           
                         
                                        }
                                        else { }
                                    }
                                    else
                                    {
                                        MessageBox.Show("invalid name");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Invalid address");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Invalid Name <<Number Cannot be a name>>");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid Operator");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Phone Number is less then 11");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex.Message.ToString());
                }
            }
            else
            {
                MessageBox.Show("Invalid input or incomplete field");
            }
        }

        private void Srijony_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void BtnRefrash_Click(object sender, EventArgs e)
        {
            textBoxcid.Text = " ";
            textBoxcname.Text = " ";
            textBoxcaddress.Text = " ";
            textBoxcphnnmbr.Text = " ";
            textBoxctype.Text = " ";
            textBoxmail.Text = "";
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select * from CustomerData";

                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.Fill;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            SqlConnection sql = new SqlConnection(conString);

            sql.Open();

            if (sql.State == ConnectionState.Open)
            {
                if (!textBoxcid.Text.ToString().Equals(""))
                {
                    string query = "delete from CustomerData where Customer_Id ='" + textBoxcid.Text.ToString() + "'";
                    SqlCommand sql1 = new SqlCommand(query, sql);
                    sql1.ExecuteNonQuery();
                    textBoxcid.Text = "";
                    textBoxcname.Text = "";
                    textBoxcaddress.Text = "";
                    textBoxcphnnmbr.Text = "";
                    textBoxctype.Text = "";
                    //SqlConnection sql1 = new SqlConnection(conString);
                    //sql.Open();
                    if (sql.State == ConnectionState.Open)
                    {
                        string query1 = "select * from CustomerData";

                        SqlCommand sql2 = new SqlCommand(query1, sql);
                        SqlDataAdapter sda = new SqlDataAdapter(query1, sql);
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        dataGridView1.DataSource = dt;
                        dataGridView1.AllowUserToAddRows = false;
                        dataGridView1.AllowUserToDeleteRows = false;
                        dataGridView1.AllowUserToResizeRows = false;
                        dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                        dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                        dataGridView1.AutoSizeColumnsMode =
                            DataGridViewAutoSizeColumnsMode.Fill;
                    }
                    MessageBox.Show("<<SUCCESS>>", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show("No Data Found");
                }
            }
            else
            {
                MessageBox.Show("Not ok");
            }
        }


        private void textBoxsearch_TextChanged(object sender, EventArgs e)
        {

            string value = textBoxsearch.Text.ToString();
            SqlConnection sql = new SqlConnection(conString);
            sql.Open();
            if (sql.State == ConnectionState.Open)
            {
                string query = "select * from CustomerData where Customer_Id like '" + value + "%'";
                SqlCommand sql1 = new SqlCommand(query, sql);
                SqlDataAdapter sda = new SqlDataAdapter(query, sql);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dataGridView1.DataSource = dt;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AutoSizeColumnsMode =
                    DataGridViewAutoSizeColumnsMode.Fill;
            }
        }
    }
}
