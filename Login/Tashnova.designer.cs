﻿namespace Login
{
    partial class Tashnova
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnupdate = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.labelcrp = new System.Windows.Forms.Label();
            this.labelnp = new System.Windows.Forms.Label();
            this.textBoxcrntps = new System.Windows.Forms.TextBox();
            this.textBoxnewps = new System.Windows.Forms.TextBox();
            this.btnsingout = new System.Windows.Forms.Button();
            this.labelcp = new System.Windows.Forms.Label();
            this.textBoxconps = new System.Windows.Forms.TextBox();
            this.btnclear = new System.Windows.Forms.Button();
            this.textBoxcc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnupdate
            // 
            this.btnupdate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnupdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnupdate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnupdate.Location = new System.Drawing.Point(186, 364);
            this.btnupdate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(97, 30);
            this.btnupdate.TabIndex = 0;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseVisualStyleBackColor = false;
            this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
            // 
            // btncancel
            // 
            this.btncancel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btncancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btncancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btncancel.Location = new System.Drawing.Point(445, 364);
            this.btncancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(97, 30);
            this.btncancel.TabIndex = 1;
            this.btncancel.Text = "Cancel";
            this.btncancel.UseVisualStyleBackColor = false;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // labelcrp
            // 
            this.labelcrp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelcrp.AutoSize = true;
            this.labelcrp.BackColor = System.Drawing.Color.Transparent;
            this.labelcrp.Font = new System.Drawing.Font("Palatino Linotype", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelcrp.ForeColor = System.Drawing.SystemColors.Control;
            this.labelcrp.Location = new System.Drawing.Point(116, 76);
            this.labelcrp.Name = "labelcrp";
            this.labelcrp.Size = new System.Drawing.Size(205, 32);
            this.labelcrp.TabIndex = 2;
            this.labelcrp.Text = "Current Password";
            // 
            // labelnp
            // 
            this.labelnp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelnp.AutoSize = true;
            this.labelnp.BackColor = System.Drawing.Color.Transparent;
            this.labelnp.Font = new System.Drawing.Font("Palatino Linotype", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelnp.ForeColor = System.Drawing.SystemColors.Control;
            this.labelnp.Location = new System.Drawing.Point(116, 139);
            this.labelnp.Name = "labelnp";
            this.labelnp.Size = new System.Drawing.Size(173, 32);
            this.labelnp.TabIndex = 3;
            this.labelnp.Text = "New Password";
            // 
            // textBoxcrntps
            // 
            this.textBoxcrntps.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxcrntps.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxcrntps.Location = new System.Drawing.Point(394, 76);
            this.textBoxcrntps.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxcrntps.Name = "textBoxcrntps";
            this.textBoxcrntps.PasswordChar = '*';
            this.textBoxcrntps.Size = new System.Drawing.Size(245, 38);
            this.textBoxcrntps.TabIndex = 4;
            // 
            // textBoxnewps
            // 
            this.textBoxnewps.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxnewps.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxnewps.Location = new System.Drawing.Point(394, 139);
            this.textBoxnewps.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxnewps.Name = "textBoxnewps";
            this.textBoxnewps.PasswordChar = '*';
            this.textBoxnewps.Size = new System.Drawing.Size(245, 38);
            this.textBoxnewps.TabIndex = 5;
            // 
            // btnsingout
            // 
            this.btnsingout.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnsingout.Location = new System.Drawing.Point(657, 11);
            this.btnsingout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnsingout.Name = "btnsingout";
            this.btnsingout.Size = new System.Drawing.Size(101, 26);
            this.btnsingout.TabIndex = 6;
            this.btnsingout.Text = "Sign Out";
            this.btnsingout.UseVisualStyleBackColor = true;
            this.btnsingout.Click += new System.EventHandler(this.btnsingout_Click);
            // 
            // labelcp
            // 
            this.labelcp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelcp.AutoSize = true;
            this.labelcp.BackColor = System.Drawing.Color.Transparent;
            this.labelcp.Font = new System.Drawing.Font("Palatino Linotype", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelcp.ForeColor = System.Drawing.SystemColors.Control;
            this.labelcp.Location = new System.Drawing.Point(116, 199);
            this.labelcp.Name = "labelcp";
            this.labelcp.Size = new System.Drawing.Size(210, 32);
            this.labelcp.TabIndex = 7;
            this.labelcp.Text = "Confirm Password";
            // 
            // textBoxconps
            // 
            this.textBoxconps.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxconps.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxconps.Location = new System.Drawing.Point(394, 199);
            this.textBoxconps.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxconps.Name = "textBoxconps";
            this.textBoxconps.PasswordChar = '*';
            this.textBoxconps.Size = new System.Drawing.Size(245, 38);
            this.textBoxconps.TabIndex = 8;
            this.textBoxconps.TextChanged += new System.EventHandler(this.textBoxconps_TextChanged);
            // 
            // btnclear
            // 
            this.btnclear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnclear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnclear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnclear.Location = new System.Drawing.Point(310, 364);
            this.btnclear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(97, 30);
            this.btnclear.TabIndex = 9;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = false;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // textBoxcc
            // 
            this.textBoxcc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBoxcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxcc.Location = new System.Drawing.Point(394, 265);
            this.textBoxcc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxcc.Name = "textBoxcc";
            this.textBoxcc.Size = new System.Drawing.Size(245, 38);
            this.textBoxcc.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(116, 265);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(218, 32);
            this.label1.TabIndex = 14;
            this.label1.Text = "Confirmation Code";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(40)))), ((int)(((byte)(27)))));
            this.button1.Enabled = false;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(432, 307);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(207, 30);
            this.button1.TabIndex = 16;
            this.button1.Text = "SEND CONFIRMATION";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Tashnova
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Login.Properties.Resources.on_the_table_at_library_blurred_background_h0rouef1g_thumbnail_full01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(770, 446);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxcc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.textBoxconps);
            this.Controls.Add(this.labelcp);
            this.Controls.Add(this.btnsingout);
            this.Controls.Add(this.textBoxnewps);
            this.Controls.Add(this.textBoxcrntps);
            this.Controls.Add(this.labelnp);
            this.Controls.Add(this.labelcrp);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnupdate);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Tashnova";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change Password";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Tashnova_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnupdate;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Label labelcrp;
        private System.Windows.Forms.Label labelnp;
        private System.Windows.Forms.TextBox textBoxcrntps;
        private System.Windows.Forms.TextBox textBoxnewps;
        private System.Windows.Forms.Button btnsingout;
        private System.Windows.Forms.Label labelcp;
        private System.Windows.Forms.TextBox textBoxconps;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.TextBox textBoxcc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}